#coding:utf-8
from twisted.internet import reactor, defer
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.python import log
from twisted.web import client
from twisted.internet.error import *
from sys import stdout
import sys

class Echo(Protocol):
    def dataReceived(self, data):
        pass
        
    def connectionLost(self, reason):
        self.d.callback({
                         "success":1
                         })
    
    def connectionMade(self):
        self.transport.loseConnection()


class EchoClientFactory(ClientFactory):
    def startedConnecting(self, connector):
        pass

    def clientConnectionLost(self, connector, reason):
        pass
    
        
    def buildProtocol(self, addr):
        p = Echo()
        p.d = self.d
        return p
        

    def clientConnectionFailed(self, connector, reason):
        self.d.callback({
                         "success":0,
                         "reason":reason
                         })

        
@defer.inlineCallbacks
def connectionHost(ip, port, timeout, bindAddress):
    f = client.HTTPClientFactory("http://iframe.ip138.com/ic.asp", timeout=30)
    reactor.connectTCP(ip, port, f, timeout=timeout, bindAddress=bindAddress)
    ret = yield f.deferred
    defer.returnValue(ret)


def test_ret(ret):
    ret = ret.decode("gb2312")  
    print ret
    
    
@defer.inlineCallbacks
def main():
    while 1:
        ret = yield connectionHost("219.153.49.149", 80, 10, ("192.168.2.221", 0))
        print ret
    

@defer.inlineCallbacks
def main1():
    while 1:
        ret = yield connectionHost("219.153.49.149", 80, 10, ("192.168.2.222", 0))
        print ret
        
if __name__ == "__main__":
    
    log.startLogging(sys.stdout)
    
    for i in range(0, 100):
        reactor.callWhenRunning(main)
        reactor.callWhenRunning(main1)
    
    reactor.run()

