"""
from twisted.internet import defer
Tests borrowed from the twisted.web.client tests.
"""
import os
from urlparse import urlparse
from twisted.internet import reactor, defer
from twisted.trial import unittest
from twisted.web import server, static, error, util
from twisted.internet import reactor, defer
from twisted.test.proto_helpers import StringTransport
from twisted.python.filepath import FilePath
from twisted.protocols.policies import WrappingFactory

from scrapy.core.downloader import webclient as client
from scrapy.http import Request, Headers


def getPage(url, contextFactory=None, *args, **kwargs):
    """Adapted version of twisted.web.client.getPage"""
    def _clientfactory(*args, **kwargs):
        timeout = kwargs.pop('timeout', 0)
        f = client.ScrapyHTTPClientFactory(Request(*args, **kwargs), timeout=timeout)
        #f.deferred.addCallback(lambda r: r.body)
        #f.deferred
        return f

    from twisted.web.client import _makeGetterFactory
    return _makeGetterFactory(url, _clientfactory,
        contextFactory=contextFactory, *args, **kwargs).deferred
        
@defer.inlineCallbacks
def main():
    d = getPage("http://www.fenzhi.com")
    response = yield d
    print response, response.body

    #print body.decode("gb2312")
    

if __name__ == "__main__":
    reactor.callWhenRunning(main)
    reactor.run()