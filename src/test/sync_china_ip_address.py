#coding:utf-8

import urllib2
import pymongo
import socket
import struct

conn = pymongo.Connection("192.168.2.202", 27017)
china_address = conn["sys"]["china_ip"]
scan_task = conn["sys"]["scan_task"]


def main(): 
    range_list = []
    with open("CN-20130305.txt") as f:
        for line in f.read().splitlines():
            ips = line.split("\t")
            data = {
                    "ip":ips[0],
                    "mask":ips[1],
                    "count":ips[2]
                    }
            range = [
                     socket.ntohl(struct.unpack("I",socket.inet_aton(ips[0]))[0]),
                     socket.ntohl(struct.unpack("I",socket.inet_aton(ips[1]))[0])
                     ]
            china_address.save(data)
            range_list += range
            
    #print range_list
    range_list.sort()
    print range_list
    
    for ip in range_list:
        print socket.inet_ntoa(struct.pack('I',socket.htonl(ip))) 
    
    task_list = []
    cnt = 0
    tmp = []
    for ip in range_list:
        cnt += 1
        tmp.append(ip)
        if cnt % 2 == 0:
            task_list.append(tmp)
            tmp = []
            
    print task_list
    
    task = {
            "name": "扫描全国6675端口",
            "port": 6675,
            "srange":task_list
            }
    scan_task.save(task)
    
    
if __name__ == "__main__":
    main()









