#coding: utf-8


from twisted.internet import reactor, defer
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from scanipd import test_proxy
    
@defer.inlineCallbacks
def main():
    
    try:
        response = yield test_proxy("1.27.2.4", 6675)
        print response
    except Exception, e:
        print e
    
    
if __name__ == "__main__":
    
    reactor.callWhenRunning(main)    
    reactor.run()


