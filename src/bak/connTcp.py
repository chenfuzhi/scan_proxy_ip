#coding:utf-8
from twisted.internet import reactor, defer
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.python import log
from sys import stdout
import sys

class Echo(Protocol):
    def dataReceived(self, data):
        stdout.write(data)

class EchoClientFactory(ClientFactory):
    def startedConnecting(self, connector):
        pass

    def buildProtocol(self, addr):
        print 'Connected.'
        print 'Resetting reconnection delay'
        self.resetDelay()
        return Echo()

    def clientConnectionLost(self, connector, reason):
        pass
        
    def buildProtocol(self, addr):
        self.d.callback({
                         "success":1
                         })

    def clientConnectionFailed(self, connector, reason):
        self.d.callback({
                         "success":0,
                         "reason":reason
                         })
        
@defer.inlineCallbacks
def connectionHost(ip, port, timeout):
    f = EchoClientFactory()
    f.d = defer.Deferred()
    reactor.connectTCP(ip, port, f, timeout=timeout)
    ret = yield f.d
    defer.returnValue(ret)



if __name__ == "__main__":
    
    log.startLogging(sys.stdout)
    connectionHost("1.2.3.6", 1111, 3).addCallback(lambda x: log.msg(x))
    reactor.run()

