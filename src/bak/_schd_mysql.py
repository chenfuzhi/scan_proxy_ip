#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()
from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import random
import json
import pymongo
import redis
import MySQLdb




class Task(object):
    '''
    '''
    def __init__(self, store):
        self.setting = store.setting
        self.store = store
        self.key_task_all = "%s:TASK" % (self.setting.KEY_PRE)
        self.key_queue_master = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.task_dict = {}

    def start(self):
        print "___________________________________________________"
        conn = pymongo.Connection()
        conn.admin.authenticate("root", "chenfuzhi")
        rows = conn.store.task.find()
        rows = list(rows)
        for row in rows:
            reactor.callLater(0, self.initTask, *(row,))
        

    @defer.inlineCallbacks
    def initTask(self, *args):
        row = args[0]
        log.msg("task:%s is init ..............." % (row["name"].encode("utf-8")))
        task_id = str(row.pop("_id"))
        task_key = "%s:%s" % (self.key_task_all, task_id)
        taskq_key = "%s:TASKQ:%s" % (self.setting.KEY_PRE, task_id)
        
        ret = yield self.store.redis.exists(task_key)
        
        print ret, '```````````````'
        if ret == 0:
            for k, v in row.iteritems():
                yield self.store.redis.hset(task_key, k, v)


        while 1:
            
            skip = 0
            step_ip = 0
            while 1:
                _filter = txmongo.filter.sort(txmongo.filter.ASCENDING("ip"))
                _find = json.loads(row["find"])
                _find["ip"] = {"$gt":step_ip}
                rows = yield self.store.mongo.store.ip.find(_find,
                                                            filter=_filter,
                                                            limit=int(row["limit"]))
                rows = list(rows)
                if len(rows) == 0:
                    break
                
                while 1:
                    queue_size = yield self.store.redis.llen(taskq_key)
                    if queue_size < row["queue"]:
                        txn = yield self.store.redis.multi()
                        for r in rows:
                            txn.lpush(taskq_key, "%s:%s:%s" % (r["ip"], 6666, task_id))
                        r = yield txn.commit()
                        
                        log.msg("task:%s, input len:%s to queue" % (row["name"].encode("utf-8"), len(rows)))
                        break
                    yield wait(60)
                
                skip += row["limit"]
                step_ip = rows[-1]["ip"]
                
                yield wait(0.01)
                
            yield wait(30)

class Scheduler(object):
    '''
    '''
    def __init__(self, store):
        self.setting = store.setting
        self.store = store
        self.key_task_all = "%s:TASK" % (self.setting.KEY_PRE)
        self.key_queue_master = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.task_dict = {}
        self.mysql = MySQLdb.connect(host='localhost',user='root',passwd='chenfuzhi',db='data',port=3306).cursor()
        
    def start(self):
        print "___________________________________________________"
        conn = pymongo.Connection()
        conn.admin.authenticate("root", "chenfuzhi")
        rows = conn.store.task.find()
        for row in rows:
            row["step_ip"] = 0
            row["end"] = 0
            self.task_dict[str(row["_id"])] = row
            
        reactor.callLater(0, self.schd_run)
        
    @defer.inlineCallbacks
    def update_run(self, *args):
        ustatus = args[0]
        unum = args[1]
        key = "update:%s" % ustatus
        while 1:
            update1len = yield self.store.redis2.llen(key)
            log.msg("begin to update%s" % ustatus)
            if update1len > unum:
                ulist = yield self.store.redis2.lrange(key, 0, unum - 1)
                ret = yield self.store.mongo.store.ip.update({"ip":{"$in":[int(x) for x in ulist]}},
                                                       {"$set":{"status":ustatus, "ts":int(time.time())}}, 
                                                        multi=True)
                yield self.store.redis2.ltrim(key, unum, -1)
            else:
                log.msg("wait to update%s" % ustatus)
                defer.returnValue(None)
                
        
    @defer.inlineCallbacks
    def schd_run(self):
        
        while 1:
            
            try:
                    
                log.msg("begin Scheduler..........")
                queue_size = yield self.store.redis2.llen(self.key_queue_master)
                
                log.msg("Scheduler..........%s" % queue_size)
                if queue_size < self.setting.SCHD_MAX_QUEUE:
                    
                    run_dict = {}
                    now = int(time.time())
                    for k, v in self.task_dict.iteritems():
                        if now - self.task_dict[k]["end"] > self.task_dict[k]["wait"]:
                            run_dict[k] = v
                    
                    weight_sum = 0
                    for k, v in run_dict.iteritems(): 
                        w1 = weight_sum
                        w2 = weight_sum + int(v["weight"])
                        weight_sum = w2
                        run_dict[k]["wrange"] = [w1, w2]
                    
                    for i in range(0, self.setting.SCHD_PER_TIMES):
                        wrand = random.randint(0, weight_sum)
                        for k, v in run_dict.iteritems(): 
                            if v["wrange"][0] <= wrand <= v["wrange"][1]:
                                ret = yield self.inputSeed(k)
                                if ret == 0:
                                    self.task_dict[k]["end"] = int(time.time())
                                    reactor.callLater(0, self.schd_run) 
                                    defer.returnValue(None)
                else:
                    yield self.update_run(*[1, 1000])
                    yield self.update_run(*[2, 100])
                    
                yield wait(0.01)
            except Exception, e:
                log.err()
                yield self.store.instatll()
                yield wait(3)
            
    @defer.inlineCallbacks
    def inputSeed(self, task_id):
        _t = self.task_dict[task_id]
        
#------------------------------------------------------------------------------ 
        sql = "select ip from ips where ip>%s and status=%s limit %s" % (_t["step_ip"], _t["find"], _t["limit"])
        self.mysql.execute(sql)
     
        rows = self.mysql.fetchall()
     
        rows = list(rows)
        if len(rows) == 0:
            self.task_dict[task_id]["step_ip"] = 0
            defer.returnValue(len(rows))
        
#------------------------------------------------------------------------------ 
        txn = yield self.store.redis.multi()
        vals = []
        size = 30
        max = len(rows)
        pages = max / size
        if max % size > 0:
            pages += 1
        for p in range(0, pages):
            start = p*size
            end = (p+1)*size
            if end >= max: end = -1
            txn.lpush(self.key_queue_master, 
                      "%s|%s|%s" % (_t["find"], 
                                    _t["port"],
                                    ",".join([str(r[0]) for r in rows[start:end]])
                                    )
                     )
            
        r = yield txn.commit()
        log.msg("successfully to Scheduler task:%s, len:%s" % (_t["name"].encode("utf-8"), 
                                                               len(rows))
                )
        self.task_dict[task_id]["step_ip"] = rows[-1][0]
        
        defer.returnValue(len(rows))
            
    
if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
        
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        log.startLogging(open('/var/log/mond.log', 'a'))
        
    else:
        log.startLogging(sys.stdout)
        
        
#     p = Process(target=por_start, args=(Task,))
#     p.start()

    p = Process(target=por_start, args=(Scheduler,))
    p.start()
    

