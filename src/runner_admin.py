#coding:utf-8
import datetime
from flask.ext import admin
from flask.ext.mongoengine import MongoEngine
from flask.ext.admin.contrib.mongoengine import ModelView
from flask import Flask
from flask.ext import admin, wtf
from flask import request
import json
import os
import sys
import psutil
import subprocess
import setting
from multiprocessing import Process


# Create application
app = Flask(__name__)

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'
app.config['MONGODB_SETTINGS'] = {'DB': 'sys'}

# Create models
db = MongoEngine()
db.init_app(app)


class Find(db.EmbeddedDocument):
    type = db.StringField(max_length=10, choices=[("mongodb", u"数据库"), ("txt", u"静态"), ("success", u"直接地址")], verbose_name =u'类型')
    code = db.StringField(max_length=10, verbose_name =u'连接内容')
    
    def __unicode__(self):
        return self.name
    
# Define mongoengine documents
class Tasks(db.Document):
    name = db.StringField(max_length=40, verbose_name =u'任务名称')
    ports = db.ListField(db.IntField(), verbose_name=u'扫描端口')
    queue = db.IntField(verbose_name =u'队列长度', default=10000)
    limit = db.IntField(verbose_name =u'查询大小', default=5000)
    prio = db.IntField(verbose_name =u'优先级', default=0)
    weight = db.IntField(verbose_name =u'权重', default=500)
    wait = db.IntField(verbose_name =u'等待时间', default=300)
    runtimes = db.IntField(verbose_name =u'运行次数', default=0)
    find = db.StringField(verbose_name =u'查找条件', max_length=1, default="0")
    def __unicode__(self):
        return self.name

def ch_find(v, c, m, p):
    return u"【%s】:【%s】" % (c["row"]["find"]["type"], c["row"]["find"]["code"])

# Customized admin views
class TaskView(ModelView):
    list_template = 'list.html'
    column_list = ('id', 'name', 'ports', 'queue', "find", "weight", "wait", "console", )             
#     column_formatters = dict(find=ch_find)

# Flask views
@app.route('/')
def index():
    return '<a href="/admin/">Click me to get to Admin!</a>'

@app.route('/task_schd/')
def task_schd():
    act = request.args.get('act', '')
    _id = request.args.get('tid', '')
    
    if act == "" or _id == "":
        return "act is none or id is none"
    
    
    TASK_PID = "/tmp/tasks/%s.pid" % _id
    TASK_LOG = "/var/log/tasks/%s.log" % _id
    if act == "status":
        try:
            pid = open(TASK_PID, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        args = ["/usr/local/bin/twistd",
                 "-y",
                 "/root/scan_proxy_ip/src/runner_task.py",
                 "--pidfile=%s" % TASK_PID,
                 "--logfile=%s" % TASK_LOG,
                 "--rundir=/root/scan_proxy_ip/src/",
                 ]
        try:
            out = subprocess.call(args)
            if out == 0:
                return '{"success":1, "msg":"ok"}'
            elif out == 1:
                return '{"success":1, "msg":"is seen already running."}'
            
        except Exception, e:
            log.err()
            return '{"success":0, "msg":"%s"}' % e
        
        
    elif act == "stop":
        try:
            pid = open(TASK_PID, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e


@app.route('/schdc/')
def schdc():
    act = request.args.get('act', '')
    if act == "status":
        try:
            pid = open(setting.SCHD_PID, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        args = ["/usr/local/bin/twistd",
                 "-y",
                 "/root/scan_proxy_ip/src/runner_schd.py",
                 "--pidfile=/tmp/schd.pid",
                 "--logfile=/var/log/schd.log",
                 "--rundir=/root/scan_proxy_ip/src/",
                 ]
        try:
            out = subprocess.call(args)
            if out == 0:
                return '{"success":1, "msg":"ok"}'
            elif out == 1:
                return '{"success":1, "msg":"is seen already running."}'
            
        except Exception, e:
            log.err()
            return '{"success":0, "msg":"%s"}' % e
        
        
    elif act == "stop":
        try:
            pid = open(setting.SCHD_PID, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e

#------------------------------------------------------------------------------ 
@app.route('/checkc/')
def checkc():
    act = request.args.get('act', '')
    if act == "status":
        try:
            pid = open(setting.CHECKIP_PID, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        args = ["/usr/local/bin/twistd",
                 "-y",
                 "/root/scan_proxy_ip/src/runner_checkip.py",
                 "--pidfile=/tmp/checkip.pid",
                 "--logfile=/var/log/checkip.log",
                 "--rundir=/root/scan_proxy_ip/src/",
                 ]
        try:
            out = subprocess.call(args)
            if out == 0:
                return '{"success":1, "msg":"ok"}'
            elif out == 1:
                return '{"success":1, "msg":"is seen already running."}'
            
        except Exception, e:
            log.err()
            return '{"success":0, "msg":"%s"}' % e
        
        
    elif act == "stop":
        try:
            pid = open(setting.CHECKIP_PID, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
        
if __name__ == '__main__':
    admin = admin.Admin(app, u'端口扫描系统')
    admin.add_view(TaskView(Tasks))
    app.debug = True
    app.run('0.0.0.0', 8000)
    
else:
    admin = admin.Admin(app, u'端口扫描系统')
    admin.add_view(TaskView(Tasks))
