#coding:utf-8

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
from twisted.web.client import *



class InputSeed:
    def __init__(self, store):
        self.store = store
        self.setting = store.setting
        self.key_queue = "IPAREA:QUEUE"
        self.key_task = "IPAREA:TASK"
        
        self.srange = [map(strip2int, x) for x in self.setting.SRANGE]
        self.max_queue = 10000
        self.pre_seeds = 1000
        self.run_count = 0
        
    @defer.inlineCallbacks
    def init(self):
        ret = yield self.store.redis.exists(self.key_task)
        if ret == 0:
            yield self.store.redis.hset(self.key_task, "ip", self.srange[0][0])
            yield self.store.redis.hset(self.key_task, "mask", self.srange[-1][-1])
    
        
    def start(self):
        reactor.callWhenRunning(self._run)
        #reactor.run()
        
    @defer.inlineCallbacks
    def _run(self):
        yield self.init()
        while 1:
            
            for r in self.srange:
                while 1:
                    queue_size = yield self.store.redis.llen(self.key_queue)
                    if queue_size < self.max_queue:
                        
                        run_ip = yield self.store.redis.hget(self.key_task, "ip")
                        if r[0] <= int(run_ip) < r[1]:
                            
                            add_num = self.pre_seeds -1
                            if (run_ip + add_num) >= r[1]:
                                add_num = r[1] - run_ip -1
                            
                            t = yield self.store.redis.multi()
                            for i in range(0, add_num):
                                ip_addr = intip2str(run_ip + i)
                                t.lpush(self.key_queue, ip_addr)
                            t.hset(self.key_task, "ip", run_ip + add_num)
                            yield t.commit()
                            log.msg("successfully input seed %s" % add_num)
                            if r[1] - 1 == run_ip:
                                index = self.srange.index(r)
                                if len(self.srange) - 1 > index:
                                    yield self.store.redis.hset(self.key_task, "ip", self.srange[index + 1][0])
                                else:
                                    yield self.store.redis.hset(self.key_task, "status", "finish")
                                    defer.returnValue(None)
                                break
                            
                        else:
                            break
                    else:
                        break
            log.msg("...........wait 3 second")
            yield wait(3)
        
            

class ScanEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "IPAREA:QUEUE"
        self.threads = threads
        self.mongo206 = None
        
    @defer.inlineCallbacks
    def init(self):
        self.mongo206 = yield txmongo.MongoConnection("192.168.2.206", 27017)
        self.mongo206 = self.mongo206["ip"]["ip_area"]
        defer.returnValue(None)
        
                
    def start(self):
        reactor.callWhenRunning(self.init)
        for i in range(0, self.threads):
            reactor.callWhenRunning(self._run)
            
            
    @defer.inlineCallbacks
    def _run(self):
        self.run_count = 0
        while 1:
            if self.mongo206 is None:
                yield wait(3)
                continue
            
            ip_addr_str = yield self.store.redis.rpop(self.key_queue)
            
            print "ip_addr_str"
            if ip_addr_str is None:
                log.msg("........wait 10 second to get ip_addr_str")
                yield wait(10)
            else:
                
                self.run_count += 1
                log.msg("test get next ip, run_count:%s" % self.run_count)
                
                log.msg(ip_addr_str)
                try:
                    url = "http://www.ip138.com/ips138.asp?ip=%s" % ip_addr_str
                    response = yield getPage(url.encode("utf-8"))
#                    本站主数据：广东省 电信</li>
                    rets = re.findall(u"本站主数据：(.*?)</li>", response.decode("gb2312"))
                    log.msg(rets[0].encode("utf-8"))
                    data = {
                            "ip":ip_addr_str,
                            "body":rets[0]
                            }
                    self.mongo206.save(data)
                    print len(response)
                except Exception, e:
                    yield self.store.redis.lpush(self.key_queue, ip_addr_str)
                    log.err()
                    
            log.msg("ScanEngine wait 3 second ......")
            yield wait(0)
            


    
por_list = []

if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    parser.add_option('--stdout', dest='stdout', action="store_true", help='run deamon', default=True)
    
    parser.add_option('--num', dest='num', help='run workers default:10', type=int, default=1)


    options, args = parser.parse_args()
    print options, args

        
        
    from twisted.python.logfile import DailyLogFile
    if options.stdout:
        log.startLogging(sys.stdout)
    else:
        log.startLogging(open('/var/log/scanipd.log', 'a'))

    p = Process(target=por_start, args=(InputSeed,))
    p.start()
    por_list.append(p)
    
    for i in range(0, options.num):
        p = Process(target=por_start, args=(ScanEngine, 1))
        p.start()
        por_list.append(p)

        

    
