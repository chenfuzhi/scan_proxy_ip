#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import urllib2
import json
import connTcp 
from twisted.internet.error import *


class ScanEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MAIN" % (self.setting.KEY_PRE)
        self.threads = threads
    
    def start(self):
        for i in range(0, self.threads):
            reactor.callLater(0, self._run)


    @defer.inlineCallbacks
    def _run(self):
        
        self.run_count = 0
        while 1:
            
            try:
                yield wait(0.001)
                task = yield self.store.redis.rpop(self.key_queue)
                if task is not None:
                    ip = yield self.store.redis.rpop("%s:QUEUE:TASK:%s" % (self.setting.KEY_PRE, task))
                    if ip is not None:
                        ip, port, status = ip.split(":")
                        ip, port = map(int, [ip, port])
                        ip = intip2str(ip)
                        self.run_count += 1
                        log.msg("test get next ip[%s:%s], run_count:%s" % 
                                (ip, port, self.run_count)
                                )
                        
                        
                        begin_time = time.time()
                        try:
                            response = yield connTcp.connectionHost(ip, port, 10)
                        except Exception, e:
                            log.err()
                            
                            if isinstance(e, ConnectionRefusedError):
                                pass
                            elif isinstance(e, TimeoutError):
                                pass
                            
                            continue
                        
                        log.msg(response)

                        try:
                            response = response.decode("gb2312")
                        except:
                            continue
                        
                        if response.find(u"您的IP是：[%s]" % ip) > -1:
                            data = {}
                            data["status"] = "o"
                            data["speed"] = time.time() - begin_time
                            try:
                                data.update(extr_area(response))
                            except Exception, e:
                                log.err()
                            data["uri"] = "%s:%s" % (ip, port)
                            data["ip"] = ip
                            data["port"] = int(port)
                            data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                            data["ts"] = int(time.time())
                            log.msg("successfully#############")     
                            yield self.store.mongo.data.ip.update({"uri":data["uri"]}, 
                                                                  data, 
                                                                  upsert=True)                 
                            if status in "1":
                                yield self.store.redis.lpush("ips:save", "%s:%s" % (strip2int(ip), "o"))
    
                else:
                    log.msg("wait 3 second.....")
                    yield wait(3)
                    
            except Exception, e:
                log.err()
                
                log.msg("wait 10 second.....")
                yield wait(30)
                yield self.store.install()

def extr_area(response):
    data = {}
    rets = re.findall(u"来自：(.*?)</center>", response)
    if len(rets) > 0:
        strs = rets[0].split(u" ")
        data["type"] = strs[-1]
        data["province"] = strs[0]
        rets = re.findall(u"(内蒙古|宁夏|北京市|上海市|重庆市|天津市|新疆|黑龙江省|广东省|福建省|浙江省|江苏省|山东省|河北省|辽宁省|吉林省|陕西省|西藏|四川省|广西|贵州省|云南省|湖南省|湖北省|江西省|安徽省|海南省|河南省|青海省|山西省|台湾省|甘肃省|中国)(.*)", "".join(strs[:-1]))
        if len(rets) > 0:
            if len(rets[0]) > 0:
                data["province"] = rets[0][0]
                if len(rets[0]) > 1:
                    data["city"] = rets[0][1]
        
    return data


    
    
por_list = []

if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
        
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        log.startLogging(open('/var/log/scanipd.log', 'a'))
    else:
        log.startLogging(sys.stdout)
        
        
    for i in range(0, 4):
        p = Process(target=por_start, args=(ScanEngine, 16500))
        p.start()
        por_list.append(p)

    
