#coding:utf-8
import cyclone.web
import sys

# from twisted.internet import epollreactor
# epollreactor.install()

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import urllib2
import json
import connTcp 
from txmongo import filter



local_ip = "124.232.163.62"
local_port = 8888
local_url = "http://%s:%s/" % (local_ip, local_port)


class CheckEngine:
    def __init__(self, store, threads=500):
        '''
        :param store:
        :param threads:########################校验线程数量
        '''
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.threads = threads
        self.queue = []
        
    @defer.inlineCallbacks
    def seed(self, _find, _wait):
        sum = 0
        while 1:
            
            try:
                skip = 0
                limit = 2000
                while 1:
                    rows = yield self.store.mongo.data.ip.find(_find, 
                                                               filter=filter.sort(filter.ASCENDING('_id')),
                                                               #fields={"uri":1},
                                                                skip=skip,
                                                                limit=limit)
                    rows = list(rows)
                    if len(rows) == 0:
                        break
                    for row in rows:
                        yield self.store.redis.lpush("tcp:success", row["uri"])
                        log.msg("input=>tcp:success: uri=%s" % row["uri"])
                        
                    log.msg("MONGO:input seed len:%s" % (len(rows)))
                    
                    skip += limit
            except:
                log.msg("ERR:#############################")
                yield wait(5)
                yield self.store.install()
                
                
            yield wait(_wait) 
            print "aaaaaaaaaaa"
    
    def start(self):
        
        reactor.callLater(0, self.seed, *({}, 30))
        for i in range(0, self.threads):
            reactor.callLater(0, self._run, i)

    @defer.inlineCallbacks
    def _run(self, worker_id):
        log.msg("#start........%s" % worker_id)
        self.run_count = 0
        while 1:
            yield wait(0.01)
            try:
                ip_str = yield self.store.redis.lpop("tcp:success")
                    
                if ip_str is not None:
                    ip_addr, port = ip_str.split(":")
                    
                    res_ok = False
                    try:
                        begin_time = time.time()
                        response = yield test_ip138(ip_addr, int(port))
                        res_ok = True
                    except Exception, e:
                        log.err()
                    
                    
                    
                    if res_ok is True and response is not None:
                        data = {}
                        data["status"] = "o"
                        data["speed"] = time.time() - begin_time
                        try:
                            data.update(extr_area(response))
                        except Exception, e:
                            log.err()
                            continue
                        data["uri"] = ip_str
                        data["ip"] = ip_addr
                        data["port"] = int(port)
                        data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                        data["ts"] = int(time.time())
                        log.msg("successfully#############")
                        ret = yield self.store.mongo["data"]["ip"].update({"uri":ip_str}, 
                                                                          data,
                                                                          upsert=True
                                                                          )
                        log.msg("######################################################")
                        
                    else:
                        yield self.store.mongo["data"]["ip"].remove({"uri":ip_str})
                else:
                    yield wait(5)
            except:
                log.msg("ERR:#############################")
                yield wait(5)
                yield self.store.install()
            

@defer.inlineCallbacks
def test_ip138(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port], timeout=120)
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：[%s]" % ip_addr) > -1:
            defer.returnValue(response.decode("gb2312"))
    defer.returnValue(None)
    
    
@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port], timeout=60)
    response = yield agent.request("GET", local_url)
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        defer.returnValue(response)
        
#         if response.decode("gb2312").find(u"您的IP是：[%s]" % ip_addr) > -1:
#             defer.returnValue(response.decode("gb2312"))
            
    defer.returnValue(None)



def extr_area(response):
    data = {}
    rets = re.findall(u"来自：(.*?)</center>", response)
    if len(rets) > 0:
        strs = rets[0].split(u" ")
        data["type"] = strs[-1]
        data["province"] = strs[0]
        rets = re.findall(u"(内蒙古|宁夏|北京市|上海市|重庆市|天津市|新疆|黑龙江省|广东省|福建省|浙江省|江苏省|山东省|河北省|辽宁省|吉林省|陕西省|西藏|四川省|广西|贵州省|云南省|湖南省|湖北省|江西省|安徽省|海南省|河南省|青海省|山西省|台湾省|甘肃省|中国)(.*)", "".join(strs[:-1]))
        if len(rets) > 0:
            if len(rets[0]) > 0:
                data["province"] = rets[0][0]
                if len(rets[0]) > 1:
                    data["city"] = rets[0][1]
        
    return data

    
por_list = []

        
@defer.inlineCallbacks
def start(*args):
    log.msg("begin install store")
    store = Store(setting)
    
    ret = yield store.install()
    if ret is False:
        log.err("store install error !!!")
        reactor.stop()
    else:
        log.msg("install successfully")
    args[0](store).start()



class MainHandler(cyclone.web.RequestHandler):
    def get(self):
        self.write(self.request.remote_ip)
#         self.write(self.request.headers)
        

os.system("ulimit -n 100000;")
if __name__ == "__main__":
    application = cyclone.web.Application([
        (r"/", MainHandler)
    ])

    log.startLogging(sys.stdout)
    reactor.listenTCP(local_port, application, interface="0.0.0.0")
    reactor.callWhenRunning(start, *(CheckEngine, ))
    reactor.run()
    
if __name__ == "__builtin__":
    
    from twisted.application.service import Application
    from twisted.python.log import ILogObserver, FileLogObserver
    from twisted.python.logfile import DailyLogFile, LogFile
    
    
    reactor.callWhenRunning(start, *(CheckEngine, ))
    application = cyclone.web.Application([
        (r"/", MainHandler)
    ])
    log.startLogging(sys.stdout)
    reactor.listenTCP(local_port, application, interface="0.0.0.0")
    application = service.Application('checkip')
    logfile = LogFile("checkip.log", "/var/log/", rotateLength=100000000000)
    application.setComponent(ILogObserver, FileLogObserver(logfile).emit)


