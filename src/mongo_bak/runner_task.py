#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()
from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import random
import json
import pymongo
import redis
import MySQLdb
from bson.json_util import default
import bson


class Task(object):
    '''
    '''
    def __init__(self, store):
        self.setting = store.setting
        self.store = store
        self.key_task_all = "%s:TASK" % (self.setting.KEY_PRE)
        self.key_queue_master = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.task_dict = {}

    def start(self, task):
        self.task_row = task
        self.key_queue = "%s:QUEUE:TASK:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        
        reactor.callLater(0, self.main_loop, *(task["find"]["type"], ))
        
        
    @defer.inlineCallbacks
    def main_loop(self, type):
        
        try:
            if type == "mongodb":
                yield self.mongodb_run()
            elif type == "txt":
                yield self.txt_run()
            elif type == "success":
                yield self.rescan_run()
        except Exception, e:
            log.err()
            log.msg("err wait 10 retry........")
            yield wait(10)
            reactor.callWhenRunning(start, *(Task,))
            
#------------------------------------------------------------------------------ 
    @defer.inlineCallbacks
    def rescan_run(self):
        coll = self.task_row["find"]["code"]
        limit = int(self.task_row["limit"])
        self.task_row["_id"] = bson.objectid.ObjectId(str(self.task_row["_id"]))
        log.msg(json.dumps(self.task_row, indent=4, default=default))
        print coll
        while 1:
            log.msg("begin task:%s.........." % str(self.task_row["_id"]))
            
            ts = 0; skip = 0;
            while 1:
                rows = yield self.store.mongo["ips"][coll].find({}, skip=skip, limit=limit)
                rows = list(rows)
                if len(rows) == 0:
                    log.msg("not more ip to input wait a circulate")
                    yield wait(int(self.task_row["wait"]))
                    break
                
                tnx = yield self.store.redis.multi()
                for row in rows:
                    ip, port = row["uri"].split(":")
                    v = "%s:%s" % (strip2int(ip), port)
                    tnx.lpush(self.key_queue, v)
                yield tnx.commit()
                
                skip += limit
                yield wait(0.01)
            yield wait(0.01)

    
#------------------------------------------------------------------------------ 
    @defer.inlineCallbacks
    def mongodb_run(self):
        
        coll = self.task_row["find"]["code"]
        limit = int(self.task_row["limit"])
        self.task_row["_id"] = bson.objectid.ObjectId(str(self.task_row["_id"]))
        log.msg(json.dumps(self.task_row, indent=4, default=default))
        print coll
        while 1:
            log.msg("begin task:%s.........." % str(self.task_row["_id"]))
            
            ipgt = 0
            while 1:
                rows = yield self.store.mongo["ips"][coll].find({"ip":{"$gt":ipgt}}, limit=limit)
                rows = list(rows)
                if len(rows) == 0:
                    log.msg("not more ip to input wait a circulate")
                    yield wait(int(self.task_row["wait"]))
                    break
                
                vals = []
                for r in rows:
                    for p in self.task_row["ports"]:
                        ipstr = "%s:%s" % (r["ip"], p)
                        vals.append(ipstr)
                        
                        if len(vals) > 1000:
                            while 1:
                                queue_size = yield self.store.redis.llen(self.key_queue)
                                if queue_size < int(self.task_row["queue"]):
                                    tnx = yield self.store.redis.multi()
                                    for v in vals:
                                        tnx.lpush(self.key_queue, v)
                                    yield tnx.commit()
                                    log.msg("1000 ip:port is input the queue.")
                                    break
                                else:
                                    log.msg("queue is max, wait 3 second....")
                                    yield wait(3)
                            vals = []
                ipgt = rows[-1]["ip"]
                yield wait(0.01)




    @defer.inlineCallbacks
    def txt_run(self):
        
        coll = self.task_row["find"]["code"]
        limit = int(self.task_row["limit"])
        
        iprange = [map(strip2int, x) for x in self.setting.SRANGE]
        while 1:
            vals = []
            for ipr in iprange:
                for ip in range(ipr[0], ipr[1]):
                    vals.append(ip)
                    if len(vals) > 1000:
                        while 1:
                            ret = yield self.store.redis.llen(self.key_queue)
                            if ret < int(self.task_row["queue"]):
                                tnx = yield self.store.redis.multi()
                                for v in vals:
                                    tnx.lpush(self.key_queue, "%s:%s" % (v, 6666))
                                yield tnx.commit()
                                log.msg("input 1000 ips：%s" % len(vals))
                                break
                            log.msg("wait 10 second.....")
                            yield wait(10)
                        vals = []
                    
            yield wait(10)
    
                
                
@defer.inlineCallbacks
def start(*args):
    log.msg("begin install store")
    store = Store(setting)
    
    ret = yield store.install()
    if ret is False:
        log.err("store install error !!!")
        reactor.stop()
    else:
        log.msg("install successfully")
        
    task = yield store.mongo.sys.tasks.find_one({"_id":ObjectId(get_task_id())})
    args[0](store).start(task)

    
    
def get_task_id():
    for arg in sys.argv:
        if arg.startswith("--pidfile"):
            return arg.split("=")[1][:-4].replace("/tmp/tasks/", "")

if __name__ == "__main__":
    log.startLogging(sys.stdout)
    reactor.callWhenRunning(start, *(Task,))
    reactor.run()
    
if __name__ == "__builtin__":
    reactor.callWhenRunning(start, *(Task,))
    application = service.Application('schd')
    

