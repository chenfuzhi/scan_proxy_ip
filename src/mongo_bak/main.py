#coding:utf-8

import pymongo
from bson.objectid import ObjectId

from flask import Flask

from flask.ext import admin, wtf

from flask.ext.admin.form import Select2Widget
from flask.ext.admin.contrib.pymongo import ModelView, filters
from flask.ext.admin.model import fields
import re
import json
from wtforms.fields import *
import os
import sys
import psutil
import subprocess
import setting
from multiprocessing import Process

# Create application
app = Flask(__name__)

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'

# Create models
conn = pymongo.Connection()
conn.admin.authenticate("root", "chenfuzhi")
db = conn.sys




# User admin
class FindForm(wtf.Form):
    type = SelectField(choices=[("mongo", "mongodb"), ("text", u"text")])
    code = StringField()


class TaskForm(wtf.Form):
    name = wtf.TextField('name', description=u'输入任务名字...', default=u"XXXXXXXXXX")
    ports = fields.InlineFieldList(wtf.IntegerField(default=6666), description=u'端口',)
    
    queue = wtf.IntegerField("queue", description=u'队列长度', default=10000)
    limit = wtf.IntegerField("limit", description=u'查询大小', default=5000)
    prio = wtf.IntegerField("prio", description=u'优先级', default=0)
    weight = wtf.IntegerField("weight", description=u'权重', default=500)
    wait = wtf.IntegerField("wait", description=u'等待时间', default=300)
    runtimes = wtf.IntegerField("runtimes", description=u'运行次数', default=0)
    find = fields.InlineFormField(FindForm)
    
#     aaaa = SelectField(choices=[("aaa", "aaa"), ("bbb", "bbb")])
#     
#     bbb = SelectMultipleField(choices=[("aaa", "aaa"), ("bbb", "bbb")])
#     ccc = RadioField(choices=[("aaa", "aaa"), ("bbb", "bbb")])
    # Form list
    form_list = fields.InlineFieldList(fields.InlineFormField(FindForm))

def redisplay(v, c, m, p):

    print c["row"]["_id"]
    return 1
     
def ch_find(v, c, m, p):
    print 1
    
    return c["row"]["find"]["type"]
    
    
class TaskView(ModelView):
    list_template = 'list.html'
    actions_template = 'actions.html'
    
    column_labels = {"console":u"控制台"}
    column_list = ('_id', 'name', 'ports', 'queue', "find", "runtimes", "console", )             
    form = TaskForm
    column_formatters = dict(find=ch_find)

    def update_model(self, form, model):
        """
            Update model helper

            :param form:
                Form instance
            :param model:
                Model instance to update
        """
        try:
            print "model", model, "\n\n\n"
            model.update(form.data)
            self.on_model_change(form, model)
            
            print form.data, "\n\n\n\n", model
            
            pk = self.get_pk_value(model)
            self.coll.update({'_id': pk}, form.data)
        except Exception, ex:
            flash(gettext('Failed to update model. %(error)s', error=str(ex)),
                  'error')
            logging.exception('Failed to update model')
            return False
        else:
            self.after_model_change(form, model, False)

        return True
    
    
# Flask views
@app.route('/')
def index():
    return '<a href="/admin/">Click me to get to Admin!</a>'


from flask import request


@app.route('/task_schd/')
def task_schd():
    act = request.args.get('act', '')
    _id = request.args.get('id', '')
    
    if act == "" or _id == "":
        return "act is none or id is none"
    
    
    TASK_PID = "/tmp/tasks/%s.pid" % _id
    TASK_LOG = "/var/log/tasks/%s.log" % _id
    if act == "status":
        try:
            pid = open(TASK_PID, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        args = ["/usr/local/bin/twistd",
                 "-y",
                 "/root/scan_proxy_ip/src/task_runner.py",
                 "--pidfile=%s" % TASK_PID,
                 "--logfile=%s" % TASK_LOG,
                 "--rundir=/root/scan_proxy_ip/src/",
                 ]
        try:
            out = subprocess.call(args)
            if out == 0:
                return '{"success":1, "msg":"ok"}'
            elif out == 1:
                return '{"success":1, "msg":"is seen already running."}'
            
        except Exception, e:
            log.err()
            return '{"success":0, "msg":"%s"}' % e
        
        
    elif act == "stop":
        try:
            pid = open(TASK_PID, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e


@app.route('/schdc/')
def schdc():
    act = request.args.get('act', '')
    if act == "status":
        try:
            pid = open(setting.SCHD_PID, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        args = ["/usr/local/bin/twistd",
                 "-y",
                 "/root/scan_proxy_ip/src/schd.py",
                 "--pidfile=/tmp/schd.pid",
                 "--logfile=/var/log/schd.log",
                 "--rundir=/root/scan_proxy_ip/src/",
                 ]
        try:
            out = subprocess.call(args)
            if out == 0:
                return '{"success":1, "msg":"ok"}'
            elif out == 1:
                return '{"success":1, "msg":"is seen already running."}'
            
        except Exception, e:
            log.err()
            return '{"success":0, "msg":"%s"}' % e
        
        
    elif act == "stop":
        try:
            pid = open(setting.SCHD_PID, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e




admin = admin.Admin(app, u'扫描器后台')
# Add views
admin.add_view(TaskView(db.tasks, u'任务管理'))
app.config["por_path"] = "/tmp/pors/"
# Start app
app.debug = True


os.system("ulimit -n 100000;")

if __name__ == '__main__':
    # Create admin
    app.run('0.0.0.0', 8080)
    
if __name__=='__builtin__':
    pass

    
    