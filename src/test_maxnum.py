#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()
from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import random
import json


class TestMax:
    def __init__(self):
        self.srange = [map(strip2int, x) for x in setting.SRANGE]
        self.ip_queue = []
        
        self.run_count = 0
        self.max_queue = 1000000
        self.run_ip = 22437777
        self.pre_seeds = 10000
        
    def get_next_ip(self):
        try:
            self.run_count += 1
            return self.ip_queue.pop()
        except:
            return None
        
    @defer.inlineCallbacks
    def start(self):
        
        for i in range(0, 10000):
            self.scan_ip()
            
        while 1:
            try:
                yield self.input_next_ips()
            except:
                pass
            yield wait()
            print "wait...", len(self.ip_queue), self.run_count
           
    @defer.inlineCallbacks
    def scan_ip(self):
        while 1:
            ip_addr = self.get_next_ip()
            
            if ip_addr is None:
                yield wait(5)
                continue
            
            print ip_addr
            port = 6666
            try:
                response = yield test_proxy2(ip_addr, int(port))
            except Exception, e:
                log.err()
                yield wait()
                continue
            if response is True:
                data = {}
                data["uri"] = "%s:%s" % (ip_addr, port)
                data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                open("/var/log/proxyip.txt", "a").write("%s:%s" % (ip_addr, int(port)))
                print "successfull scan ip:%s" % ret
                
            print ret, len(self.ip_queue), self.run_count
            
    
    
    @defer.inlineCallbacks
    def input_next_ips(self):
        if self.srange is None:
            defer.returnValue(None)
        
        try:
            for r in self.srange:
                while 1:
                    if len(self.ip_queue) < self.max_queue:
                        if r[0] <= self.run_ip < r[1]:
                            add_num = self.pre_seeds -1
                            if (self.run_ip + add_num) >= r[1]:
                                add_num = r[1] - self.run_ip -1
                            for i in range(0, add_num):
                                ip_addr = intip2str(self.run_ip + i)
                                self.ip_queue.append(ip_addr)
                            self.run_ip = self.run_ip + add_num
                                
                            if r[1] - 1 == self.run_ip:
                                index = self.srange.index(r)
                                if len(self.srange) - 1 > index:
                                    self.run_ip = self.srange[index + 1][0]
                                break
                            
                        else:
                            break
                    else:
                        break
            defer.returnValue(True)
        except Exception, e:
            log.err(e)
            defer.returnValue(False)
            
@defer.inlineCallbacks
def test_proxy2(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp', timeout=10)
    if response.code == 200:
        defer.returnValue(True)
    defer.returnValue(False)
    
    
if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
          
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        
        log.startLogging(open('/var/log/scanipd.log', 'a'))
    else:
        log.startLogging(sys.stdout)
        
    mon = TestMax()
    mon.start()
    
    reactor.run()
    
