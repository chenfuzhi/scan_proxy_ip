#coding:utf-8


import time
import urllib2
from pyquery import PyQuery
import socket
import struct


def intip2str(intip):
    return socket.inet_ntoa(struct.pack('I',socket.htonl(int(intip))))
def strip2int(strip):
    return socket.ntohl(struct.unpack("I",socket.inet_aton(str(strip)))[0])



def main():
    
    #Referer:http://ipblock.chacuo.net/view/c_CN
    
    fip = open("/var/lib/ip.db", "r+")
    
    
    for i in xrange(0, 5000000000/64):
        fip.write("0"*64)
    
    print "ip.db init finish"
    html = PyQuery(url='http://ipcn.chacuo.net/')
    lis = html("div.section_content ul.list a")
    
    
    all = 0
    for a in lis:
        href = PyQuery(a).attr("href")
    
        req = urllib2.Request('http://ipcn.chacuo.net/down/t_file=%s' % href.split("/")[-1])
        req.add_header('Referer', 'http://ipblock.chacuo.net/view/c_CN')
        r = urllib2.urlopen(req)
         
        sum = 0
        for line in r.read().split("\r\n"):
            ip0, ip1, cnt = line.split("\t")
            ip0, ip1 = map(strip2int, [ip0, ip1])
            
            for ip in xrange(ip0 + 1, ip1):
                fip.seek(ip)
                fip.write("1")
                if ip % 10000 == 0:
                    print ip
                    
                
            sum += int(cnt)
            
        
        print sum
        all += sum
        
        
    print all
    
    
    
    
    
if __name__ == "__main__":
    main()
    
    