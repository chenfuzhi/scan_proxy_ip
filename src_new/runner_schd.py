#coding:utf-8

from twisted.internet import epollreactor
try:
    epollreactor.install()
except:
    pass
from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import random
import json
import pymongo
import redis
import MySQLdb
from txmongo import ObjectId



class Schd(object):
    '''
    '''
    def __init__(self, store):
        self.setting = store.setting
        self.store = store
        self.key_task_all = "%s:QUEUE:TASK" % (self.setting.KEY_PRE)
        self.key_queue_main = "%s:QUEUE:MAIN" % (self.setting.KEY_PRE)
        self.task_dict = {}

    def start(self):
        reactor.callLater(0, self._run)
        
        
    @defer.inlineCallbacks
    def main_loop(self):
        while 1:
            try:
                yield self._run()
            except Exception, e:
                log.err()
                yield wait(3)
                reactor.callWhenRunning(start, *(Schd,))
                
                
    @defer.inlineCallbacks
    def _run(self):
        
        while 1:
            
            try:
                log.msg("begin Scheduler..........")
                queue_size = yield self.store.redis.llen(self.key_queue_main)
                log.msg("Scheduler..........%s" % queue_size)
                
                if queue_size < self.setting.SCHD_MAX_QUEUE:
                    self.task_dict = {}
                    task_list = yield self.store.redis.keys("%s*" % self.key_task_all)
                    task_list = [x.split(":")[-1] for x in task_list]
                    for task_id in task_list:
                        self.task_dict[task_id] = yield self.store.mongo.sys.tasks.find_one({"_id":ObjectId(task_id)})                             
                    
                    if len(self.task_dict) == 0:
                        log.msg("wait 3 second to get task queue has more.")
                        yield wait(3)
                        continue
                    
                    weight_sum = 0
                    for k, v in self.task_dict.iteritems():
                        w1 = weight_sum
                        w2 = weight_sum + int(v["weight"])
                        weight_sum = w2
                        self.task_dict[k]["wrange"] = [w1, w2]
                        
                    vals = []
                    while 1:
                        wrand = random.randint(0, weight_sum - 1)
                        for k, v in self.task_dict.iteritems():
                            if v["wrange"][0] <= wrand <= v["wrange"][1]:
                                vals.append(v["_id"])
                                break
                        if len(vals) % 1000 == 0:
                            print "vals mod 1000:%s" % len(vals)
                            txm = yield self.store.redis.multi()
                            for v in vals:
                                txm.lpush(self.key_queue_main, v)
                            yield txm.commit()
                            log.msg("1000 is input the queue")
                            break
                    
                else:
                    yield wait(3)
                log.msg("..................to go schd")
            except Exception, e:
                log.err()
                log.msg("ERR: %s" % e)
                yield wait(10)
                yield self.store.install()

@defer.inlineCallbacks
def start(*args):
    log.msg("begin install store")
    store = Store(setting)
    
    ret = yield store.install()
    if ret is False:
        log.err("store install error !!!")
        reactor.stop()
    else:
        log.msg("install successfully")
    args[0](store).start()


if __name__ == "__main__":
    log.startLogging(sys.stdout)
    reactor.callWhenRunning(start, *(Schd,))
    reactor.run()
    
if __name__ == "__builtin__":
    reactor.callWhenRunning(start, *(Schd,))
    application = service.Application('schd')
    

