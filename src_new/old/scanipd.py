#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()


from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import extract
import urllib2
import json


class ScanEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.threads = threads
    
    def start(self):
        for i in range(1, self.threads):
            reactor.callLater(0, self._run, i)
            
    @defer.inlineCallbacks
    def _run(self, worker_id):
        
        self.run_count = 0
        while 1:
            ip_addr_str = yield self.store.redis.rpop(self.key_queue)
            
            if ip_addr_str is None:
                log.msg("........wait 10 second to get ip_addr_str")
                yield wait(10)
            else:
                ip_addr, port, task_id = ip_addr_str.split(":")
                
                self.run_count += 1
                log.msg("test get next ip, run_count:%s" % self.run_count)
                
                try:
                    begin_time = time.time()
                    response = yield test_proxy2(ip_addr, int(port))
                    if response is True:
                        data = {}
                        data["status"] = "a"
                        data["uri"] = "%s:%s" % (ip_addr, port)
                        data["task_id"] = task_id
                        data["speed"] = time.time() - begin_time
                        data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                        data["local_ip"] = socket.gethostbyname(socket.gethostname())
                        #response = save_ip(data)
                        ret = yield self.store.mongo["data"]["ip"].save(data)
                        print "successfull scan ip:%s" % ret
                except Exception, e:
                    log.err()
                    
            yield wait(0.001)
            

@defer.inlineCallbacks
def test_proxy2(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp', timeout=10)
    if response.code == 200:
        defer.returnValue(True)
    defer.returnValue(False)
    
@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：") > -1:
            defer.returnValue(response.decode("gb2312"))
    defer.returnValue(None)


    
por_list = []

from twisted.application import service,internet


if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    parser.add_option('--stdout', dest='stdout', action="store_true", help='run deamon', default=False)
    
    parser.add_option('--num', dest='num', help='run workers default:10', type=int, default=1)


    options, args = parser.parse_args()
    print options, args

    if options.daemon:
      try:
        # Store the Fork PID
        pid = os.fork()
    
        if pid > 0:
            print 'PID: %d' % pid
            os._exit(1)
        
        pid = os.fork()
        if pid > 0:
            print "pid:%d" % pid
            sys.exit()
        
      except OSError, error:
        print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
        os._exit(1)
    
    from twisted.python.logfile import DailyLogFile
    if options.stdout:
        log.startLogging(sys.stdout)
    else:
        log.startLogging(open('/var/log/scanipd.log', 'a'))

#     for i in range(0, options.num):
#         p = Process(target=por_start, args=(ScanEngine, 60000))
#         p.start()
#         por_list.append(p)


    por_start(ScanEngine, 1)














	


        

    
