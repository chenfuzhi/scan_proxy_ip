#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import extract
import urllib2
import json

class CheckEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.threads = threads
        self.queue = []
        
    @defer.inlineCallbacks
    def seed(self, _find, _wait):
        while 1:
            
            if len(self.queue) > 0:
                yield wait(3)
                continue
            
            skip = 0
            limit = 2000
            while 1:
                rows = yield self.store.mongo.data.ip.find(_find, skip=skip, limit=limit)
                if len(list(rows)) == 0:
                    break
                for row in rows:
                    self.queue.append(row)
                log.msg("input seed %s, len:%s" % (json.dumps(_find), len(list(rows))))
                skip += limit
            yield wait(_wait) 
            
    
    def start(self):
        self.seed({"status":"a"}, 3)
        self.seed({"status":"o"}, 60)
        self.seed({"status":"d"}, 300)
        
        for i in range(0, self.threads):
            reactor.callLater(0, self._run, i)
            
    @defer.inlineCallbacks
    def _run(self, worker_id):
        
        self.run_count = 0
        while 1:
            
            try:
                row = self.queue.pop()
            except:
                row = None
                
            if row is not None:
                ip_addr, port = row["uri"].split(":")
                
                res_ok = False
                try:
                    begin_time = time.time()
                    response = yield test_proxy(ip_addr, int(port))
                    res_ok = True
                except Exception, e:
                    log.err()
                
                if res_ok is True:
                    if row["status"] == "o":
                        continue
                    data = {}
                    data["status"] = "o"
                    data["speed"] = time.time() - begin_time
                    try:
                        data.update(extr_area(response))
                    except Exception, e:
                        log.err()
                        
                    data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                    data["ts"] = int(time.time())
                    row.update(data)
                    ret = yield self.store.mongo["data"]["ip"].update({"_id":row["_id"]}, row)
                else:
                    if row.has_key("ts") and row["ts"] < int(time.time()) - 30 * 86400:
                        yield self.store.mongo["data"]["ip"].remove({"_id":row["_id"]})
                    else:
                        ret = yield self.store.mongo["data"]["ip"].update({"_id":row["_id"]}, 
                                                                          {"$set":{"status":"d"}}
                                                                          )
            else:
                yield wait(5)
            
            yield wait(3)
            
            
    
@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：") > -1:
            defer.returnValue(response.decode("gb2312"))
            
    defer.returnValue(None)



def extr_area(response):
    data = {}
    rets = re.findall(u"来自：(.*?)</center>", response)
    if len(rets) > 0:
        strs = rets[0].split(u" ")
        data["type"] = strs[-1]
        rets = re.findall(u"(内蒙古|宁夏|北京市|上海市|重庆市|天津市|新疆|黑龙江省|广东省|福建省|浙江省|江苏省|山东省|河北省|辽宁省|吉林省|陕西省|西藏|四川省|广西|贵州省|云南省|湖南省|湖北省|江西省|安徽省|海南省|河南省|青海省|山西省|台湾省|甘肃省)(.*)", "".join(strs[:-1]))
        if len(rets) > 0:
            if len(rets[0]) > 0:
                data["province"] = rets[0][0]
                if len(rets[0]) > 1:
                    data["city"] = rets[0][1]
    return data



    
por_list = []

if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    parser.add_option('--stdout', dest='stdout', action="store_true", help='run deamon', default=False)
    
    parser.add_option('--num', dest='num', help='run workers default:10', type=int, default=1)


    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
      try:
        # Store the Fork PID
        pid = os.fork()
    
        if pid > 0:
          print 'PID: %d' % pid
          os._exit(0)
      except OSError, error:
        print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
        os._exit(1)
        
    from twisted.python.logfile import DailyLogFile
    if options.stdout:
        log.startLogging(sys.stdout)
    else:
        log.startLogging(open('/var/log/checkip.log', 'a'))

    for i in range(0, options.num):
        p = Process(target=por_start, args=(CheckEngine, 10000))
        p.start()
        por_list.append(p)

        

    
