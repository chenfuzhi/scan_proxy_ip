#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()
from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import random
import json
import pymongo
import redis
import MySQLdb




class Task(object):
    '''
    '''
    def __init__(self, store):
        self.setting = store.setting
        self.store = store
        self.key_task_all = "%s:TASK" % (self.setting.KEY_PRE)
        self.key_queue_master = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.task_dict = {}

    def start(self):
        reactor.callLater(0, self.initTask)
        
        
    @defer.inlineCallbacks
    def initTask(self):

        iprange = [map(strip2int, x) for x in self.setting.SRANGE]
        
        
        while 1:
            
            vals = []
            for ipr in iprange:
                for ip in range(ipr[0], ipr[1]):
                    vals.append(ip)
                    if len(vals) > 1000:
                        while 1:
                            ret = yield self.store.redis.llen(self.key_queue_master)
                            if ret < 100000:
                                tnx = yield self.store.redis.multi()
                                for v in vals:
                                    tnx.lpush(self.key_queue_master, intip2str(v))
                                yield tnx.commit()
                                log.msg("input 1000 ips：%s" % len(vals))
                                break
                            log.msg("wait 10 second.....")
                            yield wait(10)
                        vals = []
                    
            yield wait(10)
    
if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
        
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        log.startLogging(open('/var/log/mond.log', 'a'))
        
    else:
        log.startLogging(sys.stdout)
        
        
    p = Process(target=por_start, args=(Task,))
    p.start()

#     p = Process(target=por_start, args=(Scheduler,))
#     p.start()
    

