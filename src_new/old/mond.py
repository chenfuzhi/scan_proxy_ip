#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()
from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import random
import json



class InputSeed:
    def __init__(self, store, task):
        self.store = store
        self.setting = store.setting
        self.task = task
        self.key_task = "%s:TASK:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.key_queue = "%s:QUEUE:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.srange = [map(strip2int, x) for x in task["srange"]]
        
        self.max_queue = int(task["max_queue"])
        self.pre_seeds = int(task["pre_seeds"])
        self.run_count = 0
        
        
    def quit_hander(self, signal, frame):
        pass
        
    def get_next_ip(self):
        try:
            return self.ip_queue.pop()
        except:
            return None
    
    @defer.inlineCallbacks
    def input_next_ips(self):
        if self.srange is None:
            defer.returnValue(None)
        
        try:
            for r in self.srange:
                while 1:
                    yield self.store.redis.hset(self.key_task, "status", "running")
                    queue_size = yield self.store.redis.llen(self.key_queue)
                    #log.msg("%s:%s" % (queue_size, self.setting.MAX_WORKER_QUEUE))
                    if queue_size < self.max_queue:
                        
                        run_ip = yield self.store.redis.hget(self.key_task, "ip")
                        #log.msg(run_ip)
                        if r[0] <= int(run_ip) < r[1]:
                            
                            add_num = self.pre_seeds -1
                            if (run_ip + add_num) >= r[1]:
                                add_num = r[1] - run_ip -1
                            
                            t = yield self.store.redis.multi()
                            for i in range(0, add_num):
                                #log.msg("%s, %s, %s" % (r[0], run_ip + i, r[1]))
                                #print type(run_ip)
                                ip_addr = intip2str(run_ip + i)
                                q_str = "%s:%s:%s" % (ip_addr, self.task["port"], str(self.task["_id"]))
                                t.lpush(self.key_queue, q_str)
                            t.hset(self.key_task, "ip", run_ip + add_num)
                            
                            yield t.commit()
                            if r[1] - 1 == run_ip:
                                index = self.srange.index(r)
                                if len(self.srange) - 1 > index:
                                    yield self.store.redis.hset(self.key_task, "ip", self.srange[index + 1][0])
                                else:
                                    yield self.store.redis.hset(self.key_task, "status", "finish")
                                break
                            
                        else:
                            break
                    else:
                        break
            defer.returnValue(True)
        except Exception, e:
            log.err(e)
            defer.returnValue(False)
            

            

    @defer.inlineCallbacks
    def initTask(self):

        data = {
                "ip":self.srange[0][0],
                "mask":self.srange[-1][-1],
                "task_id":str(self.task["_id"]),
                "port":self.task["port"],
                "name":self.task["name"],
                "status":"ready",
                "weight": int(self.task["weight"]),
                "queue":int(self.task["max_queue"])
                
                }
        log.msg(data)
        try:
            txn = yield self.store.redis.multi()
            for k, v in data.iteritems():
                txn.hset(self.key_task, k, v)
            r = yield txn.commit()
            defer.returnValue(True)
        except Exception, e:
            log.err(e)
            defer.returnValue(False)
        
    @defer.inlineCallbacks
    def stop(self):
        yield self.store.redis.hset(self.key_task, "status", "finish")
    
    def start(self):
        reactor.callWhenRunning(self._run)
        #reactor.run()
        
    @defer.inlineCallbacks
    def _run(self):
        is_first = True
        while 1:
            task_key = yield self.store.redis.hgetall(self.key_task)
            
            if task_key.has_key("status"):
                if task_key["status"] in ["running", "ready"]:
                    yield self.input_next_ips()
                    
                elif task_key["status"] == "finish":
                    self.run_count += 1
                    if int(self.task["run_times"]) == 0:
                        ret = yield self.initTask()
                        
            else:
                if is_first is True:
                    ret = yield self.initTask()
                    is_first = False
                else:
                    defer.returnValue(None)
                    reactor.stop()
            
            log.msg(task_key)
            log.msg("........wait 3 second.")
            yield wait(3)



class Scheduler(object):
    '''
    
    '''
    def __init__(self, store):
        self.setting = store.setting
        self.store = store
        self.key_task_all = "%s:TASK" % (self.setting.KEY_PRE)
        self.key_queue_master = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.task_dict = {}
    
        
    def start(self):
        reactor.callLater(0, self._run)
    
    @defer.inlineCallbacks
    def _run(self):
        log.msg("Scheduler is start ...............")
        while 1:
            log.msg("begin Scheduler..........")
            queue_size = yield self.store.redis.llen(self.key_queue_master)
            
            log.msg("Scheduler..........%s" % queue_size)
            if queue_size < self.setting.SCHD_MAX_QUEUE:
                self.task_dict = {}
                task_list = yield self.store.redis.keys("%s*" % self.key_task_all)
                task_list = [x.split(":")[-1] for x in task_list]
                for task_id in task_list:
                    key = "%s:TASK:%s" % (self.setting.KEY_PRE, task_id)
                    self.task_dict[task_id] = yield self.store.redis.hgetall(key)
                
                if len(self.task_dict) == 0:
                    yield wait(3)
                    continue
                
                weight_sum = 0
                for k, v in self.task_dict.iteritems():
                    w1 = weight_sum
                    w2 = weight_sum + int(v["weight"])
                    weight_sum = w2
                    self.task_dict[k]["wrange"] = [w1, w2]
                
                for i in range(0, self.setting.SCHD_PER_TIMES):
                    if len(self.task_dict) == 0: break
                    wrand = random.randint(0, weight_sum - 1)
                    for k, v in self.task_dict.iteritems():
                        if v["wrange"][0] <= wrand < v["wrange"][1]:
                            queue_key = "%s:QUEUE:%s" % (self.setting.KEY_PRE, v["task_id"])
                            t = yield self.store.redis.multi()
                            for ii in xrange(0, self.setting.SCHD_NUM):
                                t.rpoplpush(queue_key, self.key_queue_master)
                            yield t.commit()
                            log.msg("successfully to Scheduler task:%s" % v["task_id"])
                            break
                        else:
                            #log.msg("not match task:%s" % wrand)
                            pass
                    
                    yield wait(0)
                
                if len(self.task_dict) == 0:# 没有待调用的queue
                    yield wait(10) 
                    
                yield wait(0)
            else:
                yield wait(10)
            log.msg("..................to go schd")


import urllib2

class MonitorTask(object):
    '''
    
    '''
    def __init__(self, setting):
        import redis
        import pymongo
        
        self.setting = setting
        
        self.mongo = pymongo.Connection(host=self.setting.MONGO_HOST, port=self.setting.MONGO_PORT)
        self.redis = redis.Redis(host=self.setting.REDIS_HOST, 
                                 port=self.setting.REDIS_PORT,
                                 password="chenfuzhi")
        
        self.seeders = {}
        self.key_task_all = "%s:TASK" % (self.setting.KEY_PRE)
        
        self.api_pre = "http://42.96.168.234:9090/"
        
        
    def restore_task(self):
        task_list = self.redis.keys("%s*" % self.key_task_all)
        for task_key in task_list:
            task_id = task_key.split(":")[-1]
            self.start_task(task_id)
    
    
    def start(self):
        self.restore_task()
        while 1:
            #yield self.save_status()
            ret = urllib2.urlopen("%s%s" % (self.api_pre, "command/")).read()
            
            ret = json.loads(ret)
            
            if ret is not None:
                if ret["type"] == "scanip":
                    if ret["cmd"] == "start":#
                        self.start_task(ret["task_id"])
                    elif ret["cmd"] == "stop":#
                        self.stop_task(ret["task_id"])
                    
                elif cmd["type"] == "XXXXXXXXXX":
                    pass
                else:
                    log.err(" MonitorTaskunknown cmd type!!")
            else:
                log.msg("MonitorTask wait 10 second.")
                self.save_status()
                time.sleep(300)
                
    
    def save_status(self):
        task_status = {
                       "htime": time.strftime("%Y-%m-%d %H:%I:%S"),
                       "task_list":[]
                       }
        task_list = self.redis.keys("%s*" % self.key_task_all)
        for task_key in task_list:
            task_info = self.redis.hgetall(task_key)
            task_status["task_list"].append(task_info)

        req = urllib2.Request("%s%s" % (self.api_pre, "save_status/"))
        req.add_header("Content-Type", "application/json")
        response = urllib2.urlopen(req, data=json.dumps(task_status)).read()
        print "#########save_status:", response
        
    
    def start_task(self, task_id):
        from bson.objectid import ObjectId
        ret = urllib2.urlopen("%s%s" % (self.api_pre, "scan_task/%s" % task_id)).read()
        task = json.loads(ret)
        #log.msg(task["name"].encode("utf-8"))
        self.seeders[task_id] = Process(target=por_start, args=(InputSeed, task)) 
        self.seeders[task_id].start()


    def stop_task(self, task_id):
        self.seeders[task_id].stop()
        del self.seeders[task_id]
    

  

run_ps = []                
    
    
if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    parser.add_option('--stdout', dest='stdout', action="store_true", help='run deamon', default=False)

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
      try:
        # Store the Fork PID
        pid = os.fork()
    
        if pid > 0:
          print 'PID: %d' % pid
          os._exit(0)
      except OSError, error:
        print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
        os._exit(1)
        
    #from twisted.python.logfile import DailyLogFile

    if options.stdout:
        log.startLogging(sys.stdout)
    else:
        log.startLogging(open('/var/log/mond.log', 'a'))
        

    p = Process(target=por_start, args=(Scheduler,))
    p.start()

    mon = MonitorTask(setting)
    mon.start()

    

