#coding:utf-8
from twisted.internet import reactor, defer
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.python import log
from sys import stdout
import sys

class Echo(Protocol):
    def dataReceived(self, data):
        pass
        
    def connectionLost(self, reason):
        self.d.callback({
                         "success":1
                         })
    
    def connectionMade(self):
        self.transport.loseConnection()


class EchoClientFactory(ClientFactory):
    def startedConnecting(self, connector):
        pass

    def clientConnectionLost(self, connector, reason):
        pass
    
        
    def buildProtocol(self, addr):
        p = Echo()
        p.d = self.d
        return p
        

    def clientConnectionFailed(self, connector, reason):
        self.d.callback({
                         "success":0,
                         "reason":reason
                         })
        
@defer.inlineCallbacks
def connectionHost(ip, port, timeout):
    f = EchoClientFactory()
    f.d = defer.Deferred()
    reactor.connectTCP(ip, port, f, timeout=timeout)
    ret = yield f.d
    defer.returnValue(ret)



if __name__ == "__main__":
    
    log.startLogging(sys.stdout)
    connectionHost("163.com", 6666, 10).addCallback(lambda x: log.msg(x))
    reactor.run()

