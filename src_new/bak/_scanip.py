#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import extract
import urllib2
import json
from twisted.internet.error import *
import connTcp 
from twisted.internet.error import TimeoutError
from twisted.python.failure import Failure


class ScanEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.threads = threads
        self.ips = []
    
    def start(self):
        for i in range(0, self.threads):
            reactor.callLater(0, self._run, i)

        reactor.callLater(0, self.seed_run)
        
        
    @defer.inlineCallbacks
    def seed_run(self):
        
        while 1:
            if len(self.ips) < 1000000:
                ips0 = yield self.store.redis.lpop("ips:0")
                for i in ips0.split(","):
                    self.ips.append(i)
            else:
                log.msg("wait 10 second....")
                yield wait(10)
                
    
        
        
    @defer.inlineCallbacks
    def _run(self, worker_id):
        
        self.run_count = 0
        while 1:
            
            try:
                
                if len(self.ips) > 0:
                    ip = self.ips.pop()
                    for port in [6666, 6675, 6668, 6670]:
                        
                        ret = yield connTcp.connectionHost(ip, port, 10)
                        log.msg(ret)
                        
                        if ret["success"] == 1:
                            self.store.redis.lpush("update:2", strip2int(ip))
                            begin_time = time.time()
                            response = yield test_proxy2(ip, port)
                            if response is True:
                                data = {}
                                data["status"] = "a"
                                data["uri"] = "%s:%s" % (ip, port)
                                data["speed"] = time.time() - begin_time
                                data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                                ret = yield self.store.mongo["data"]["ip"].save(data)
                                print "successfull scan ip:%s" % ret
                            
                        else:
                            reason =  ret["reason"].getErrorMessage()
                            if "User timeout caused connection failure" in reason:
                                pass
                            else:
                                if status != "1":
                                    self.store.redis.lpush("update:1", strip2int(ip))
                            
                            log.msg(reason)
            except Exception, e:
                log.err()
                self.store.install()
                yield wait(3)
            

@defer.inlineCallbacks
def test_proxy2(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp', timeout=10)
    if response.code == 200:
        defer.returnValue(True)
    defer.returnValue(False)
    


    
por_list = []

if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    
    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
        
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        log.startLogging(open('/var/log/scanipd.log', 'a'))
    
    else:
        log.startLogging(sys.stdout)
        

    for i in range(0, 1):
        p = Process(target=por_start, args=(ScanEngine, 1))
        p.start()
        por_list.append(p)

        

    
