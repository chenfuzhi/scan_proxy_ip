#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import urllib2
import json
import connTcp 

class ScanEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MASTER" % (self.setting.KEY_PRE)
        self.threads = threads
    
    def start(self):
        for i in range(0, self.threads):
            reactor.callLater(0, self._run, i)
            
    @defer.inlineCallbacks
    def _run(self, worker_id):
        
        self.run_count = 0
        while 1:
            
            try:
                ip = yield self.store.redis.rpop(self.key_queue)
                
                if ip is None:
                    log.msg("........wait 10 second to get ip_addr_str")
                    yield wait(10)
                else:
                    
                    port = 6666
                    self.run_count += 1
                    log.msg("test get next ip, run_count:%s" % self.run_count)
                    ret = yield connTcp.connectionHost(ip, port, 10)
                    log.msg(ret)
                    
                    if ret["success"] == 1:
                        yield self.store.mongo["ips"]["tcp6666"].save({"ip":strip2int(ip)})
                        begin_time = time.time()
                        response = yield test_proxy2(ip, port)
                        if response is True:
                            data = {}
                            data["status"] = "a"
                            data["uri"] = "%s:%s" % (ip, port)
                            data["speed"] = time.time() - begin_time
                            data["time"] = time.strftime("%Y-%m-%d %H:%I:%S")
                            ret = yield self.store.mongo["data"]["ip"].save(data)
                            print "successfull scan ip:%s" % ret
                        
                    else:
                        reason =  ret["reason"].getErrorMessage()
                        if "User timeout caused connection failure" in reason:
                            yield self.store.mongo.ips.timeout.save({"ip":strip2int(ip)})
                        else:
                            yield self.store.mongo.ips.refused.save({"ip":strip2int(ip)})
                                        
                yield wait(0.001)
                
            except Exception, e:
                log.err()
                yield self.store.install()
                yield wait(3)

@defer.inlineCallbacks
def test_proxy2(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp', timeout=10)
    if response.code == 200:
        defer.returnValue(True)
    defer.returnValue(False)
    
@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：") > -1:
            defer.returnValue(response.decode("gb2312"))
    defer.returnValue(None)

    
por_list = []

if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
        
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        log.startLogging(open('/var/log/scanipd.log', 'a'))
    else:
        log.startLogging(sys.stdout)
        
        

    for i in range(0, 4):
        p = Process(target=por_start, args=(ScanEngine, 16000))
        p.start()
        por_list.append(p)

        

    
