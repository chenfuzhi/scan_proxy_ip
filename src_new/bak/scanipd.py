#coding:utf-8

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os


#def signal_handler(signal, frame):
#        print 'You pressed Ctrl+C!'
#        sys.exit(0)
#signal.signal(signal.SIGINT, signal_handler)
#print 'Press Ctrl+C'
#signal.pause()




class BeginningPrinter(Protocol):
    def __init__(self, finished):
        self.finished = finished
        self.remaining = 1024 * 100
        self.body = ""

    def dataReceived(self, bytes):
        self.body += bytes

    def connectionLost(self, reason):
        print 'Finished receiving body:', reason.getErrorMessage()
        self.finished.callback(self.body)



class Store(object):
    def __init__(self, setting):
        self.setting = setting
        self.mongo = None
        self.redis = None
    
    @defer.inlineCallbacks
    def instatll(self):
        try:
            self.mongo = yield txmongo.MongoConnection(host=self.setting.MONGO_HOST, port=self.setting.MONGO_PORT)
            self.redis = yield txredisapi.Connection(host=self.setting.REDIS_HOST, port=self.setting.REDIS_PORT)
            ret = yield self.mongo.admin.authenticate(self.setting.MONGO_USER, self.setting.MONGO_PWD)
            if ret == False:
                print ret
                reactor.stop()
            defer.returnValue(True)
        except Exception, e:
            defer.returnValue(False)

    
def wait(second):
    d = defer.Deferred()
    reactor.callLater(second, d.callback, None)
    return d
        
class BaseWorker(object):
    def __init__(self, store, task):
        self.store = store
        self.setting = store.setting
        self.task = task
        self.key_task = "%s:TASK:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.key_queue = "%s:QUEUE:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.key_success = "%s:SUCCESS:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.srange = task["srange"]

class InputSeed(BaseWorker):
    def __init__(self, store, task):
        BaseWorker.__init__(self, store, task)
        self.pre_num = self.setting.SEED_PER_NUM
        #signal.signal(signal.SIGINT, self.quit_hander)
        
        
    def quit_hander(self, signal, frame):
        pass
        
    def get_next_ip(self):
        try:
            return self.ip_queue.pop()
        except:
            return None
    
    @defer.inlineCallbacks
    def input_next_ips(self):
        if self.srange is None:
            defer.returnValue(None)
        
        try:
            for r in self.srange:
                while 1:
                    queue_size = yield self.store.redis.llen(self.key_queue)
                    log.msg("%s:%s" % (queue_size, self.setting.MAX_WORKER_QUEUE))
                    if queue_size < self.setting.MAX_WORKER_QUEUE:
                        
                        run_ip = yield self.store.redis.hget(self.key_task, "ip")
                        log.msg(run_ip)
                        if r[0] <= int(run_ip) < r[1]:
                            
                            log.msg("aaaaaaaaaaaaaaaaaaaaaaaa")
                            add_num = self.pre_num -1
                            if (run_ip + add_num) >= r[1]:
                                add_num = r[1] - run_ip -1
                            
                            t = yield self.store.redis.multi()
                            for i in range(0, add_num):
                                log.msg("%s, %s, %s" % (r[0], run_ip + i, r[1]))
                                print type(run_ip)
                                ip_addr = socket.inet_ntoa(struct.pack('I',socket.htonl(run_ip + i)))
                                t.lpush(self.key_queue, ip_addr)
                            t.hset(self.key_task, "ip", run_ip + add_num)
                            
                            yield t.commit()
                            if r[1] - 1 == run_ip:
                                index = self.srange.index(r)
                                if len(self.srange) - 1 > index:
                                    yield self.store.redis.hset(self.key_task, "ip", self.srange[index + 1][0])
                                else:
                                    yield self.store.redis.hset(self.key_task, "status", "finish")
                                break
                            
                        else:
                            break
                    else:
                        break
            defer.returnValue(True)
        except Exception, e:
            log.err(e)
            defer.returnValue(False)

            

    @defer.inlineCallbacks
    def task_status(self):
        key = yield self.store.redis.exists(self.key_task)
        ret = {
               "status":"normal"
               }
        if key == 0:
            ret["status"] = "none"
        else:
            int_ip = yield self.store.redis.hget(self.key_task, "ip")
            int_mask = yield self.store.redis.hget(self.key_task, "mask")
            if int_ip is None or int_mask is None:
                ret["status"] = "none"
            elif int_ip == int_mask:
                ret = yield self.store.redis.hgetall(self.key_task)
                ret["status"] = "finish"
        
        defer.returnValue(ret)
            

    @defer.inlineCallbacks
    def initTask(self):
        
        data = {
                "ip":self.srange[0][0],
                "mask":self.srange[-1][-1],
                "task_id":str(self.task["_id"]),
                "port":self.task["port"],
                "name":self.task["name"],
                "status":"ready"
                }
        log.msg(data)
        try:
            txn = yield self.store.redis.multi()
            for k, v in data.iteritems():
                txn.hset(self.key_task, k, v)
            r = yield txn.commit()
            defer.returnValue(True)
        except Exception, e:
            log.err(e)
            defer.returnValue(False)
        
    def start(self):
        reactor.callLater(0, self._run)
    
    @defer.inlineCallbacks
    def _run(self):
        while 1:
            task_key = yield self.task_status()
            if task_key["status"] == "none":
                ret = yield self.initTask()
                if ret is False:
                    pass
                
            elif task_key["status"] == "finish":
                pass
                
            elif task_key["status"] == "normal":
                yield self.input_next_ips()
            
            log.msg(task_key)
            log.msg("........wait 3 second.")
            yield wait(3)


class ScanEngine(BaseWorker):
    def __init__(self, store, task, threads=1000):
        BaseWorker.__init__(self, store, task)
        self.threads = threads
    
    def start(self):
        for i in range(1, self.threads):
            reactor.callLater(0, self._run, i)
            
    @defer.inlineCallbacks
    def _run(self, worker_id):
        
        self.run_count = 0
        while 1:
            ip_addr = yield self.store.redis.lpop(self.key_queue)
            if ip_addr is None:
                log.msg("........wait 10 second to get ip_addr")
                yield wait(10)
            else:
                self.run_count += 1
                log.msg("test get next ip, run_count:%s" % self.run_count)
                
                try:
                    response = yield test_proxy(ip_addr, self.task["port"])
                    if response is not None:
                        print response, "ok", 
                        yield self.store.redis.lpush(self.key_success, "%s:%s" % (ip_addr, self.task["port"]))
                        data = {}
                        data["time"] = int(time.time())
                        data["host"] = ip_addr
                        data["port"] = self.task["port"]
                        rets = re.findall(u"来自：(.*?)</center>", response)
                        data["area"] = rets[0]
                        yield self.store.mongo["web"]["ip"].save(data)
                        print "successfull scan ip:%s" % ip_addr
                except Exception, e:
                    log.err()
                    
            yield wait(0.001)
            
@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：") > -1:
            defer.returnValue(response.decode("gb2312"))
    defer.returnValue(None)




    
    
if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    parser.add_option('--stdout', dest='stdout', action="store_true", help='run deamon', default=False)

    # parameters
    parser.add_option('-t', '--run_type', dest='run_type', help=u'run_type, master / worker : default:worker', default='worker')
    parser.add_option('-p', '--processs', dest='process', help='process num', type='int', default=1)

    if len(sys.argv)==1:
        parser.print_help()
        sys.exit()

    options, args = parser.parse_args()
    print options, args

    if options.run_type in ["worker", "master"]:
        if options.run_type == "master" and options.process > 1:
            print "master must 1 process."
            sys.exit()
            
        if options.daemon:
          try:
            # Store the Fork PID
            pid = os.fork()
        
            if pid > 0:
              print 'PID: %d' % pid
              os._exit(0)
          except OSError, error:
            print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
            os._exit(1)
            
        from twisted.python.logfile import DailyLogFile
        if options.stdout:
            log.startLogging(sys.stdout)
        else:
            log.startLogging(open('/var/log/scanipd.log', 'a'))
            
        pros = []
        for i in range(options.process):
            p2 = Process(target=main, args=(options.run_type,))
            p2.start()
            pros.append(p2)
    else:
        print "run_type must (worker/master)"
        sys.exit()
        


    

