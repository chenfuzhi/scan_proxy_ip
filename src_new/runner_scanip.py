#coding:utf-8

from twisted.internet import epollreactor
epollreactor.install()

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
from common import *
import re
import urllib2
import json
import connTcp 

class ScanEngine:
    def __init__(self, store, threads=1000):
        self.store = store
        self.setting = store.setting
        self.key_queue = "%s:QUEUE:MAIN" % (self.setting.KEY_PRE)
        self.threads = threads
    
    def start(self):
        for i in range(0, self.threads):
            reactor.callLater(0, self._run)


    @defer.inlineCallbacks
    def _run(self):
        
        self.run_count = 0
        while 1:
            
            try:
                yield wait(0.001)
                task = yield self.store.redis.rpop(self.key_queue)
                if task is not None:
                    ip = yield self.store.redis.rpop("%s:QUEUE:TASK:%s" % (self.setting.KEY_PRE, task))
                    if ip is not None:
                        ip, port = map(int, ip.split(":"))
                        ip = intip2str(ip)
                        self.run_count += 1
                        log.msg("test get next ip, run_count:%s" % self.run_count)
                        
                        try:
                            ret = yield connTcp.connectionHost(ip, port, 30)
                        except:
                            continue
                        log.msg(ret)
                        
                        if ret["success"] == 1:
                            data = {
                                    "ts":int(time.time()),
                                    "uri": "%s:%s" % (ip, port),
                                    "port": port,
                                    "ip":ip
                                    }
                            ret = yield self.store.mongo["ips"]["success"].update({"uri":data["uri"]}, 
                                                                                  data, 
                                                                                  upsert=True
                                                                                  )
                            yield self.store.redis.lpush("tcp:success", data["uri"])
                            print "successfull scan ip:%s" % data["uri"]

                        else:
                            reason =  ret["reason"].getErrorMessage()
                            if "User timeout caused connection failure" in reason:
                                yield self.store.mongo.ips.timeout.save({"ip":strip2int(ip)})
                            else:
                                yield self.store.mongo.ips.refused.save({"ip":strip2int(ip)})
                else:
                    log.msg("wait 3 second.....")
                    yield wait(3)
                    
            except:
                log.msg("wait 10 second.....")
                yield wait(10)
                yield self.store.install()
    

@defer.inlineCallbacks
def test_proxy2(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp', timeout=10)
    if response.code == 200:
        defer.returnValue(True)
    defer.returnValue(False)

@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：") > -1:
            defer.returnValue(response.decode("gb2312"))
    defer.returnValue(None)


    
    
por_list = []

if __name__ == "__main__":

    from optparse import OptionParser
    
    parser = OptionParser(usage='usage: %prog [options]')
    # commands
    parser.add_option('--daemon', dest='daemon', action="store_true", help='run deamon', default=False)
    

    options, args = parser.parse_args()
    print options, args

        
    if options.daemon:
        try:
          # Store the Fork PID
          pid = os.fork()
        
          if pid > 0:
            print 'PID: %d' % pid
            os._exit(0)
        except OSError, error:
          print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
          os._exit(1)
        log.startLogging(open('/var/log/scanipd.log', 'a'))
    else:
        log.startLogging(sys.stdout)
        
        
    for i in range(0, 4):
        p = Process(target=por_start, args=(ScanEngine, 16500))
        p.start()
        por_list.append(p)

    
