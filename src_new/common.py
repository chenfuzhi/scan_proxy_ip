#coding:utf-8

from twisted.application import internet, service
from twisted.internet import reactor, defer
from twisted.python import log
import setting
import txmongo
import txredisapi
import time
import sys
import socket
import struct
from txmongo import ObjectId
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web import client
import subprocess
from twisted.internet.protocol import Protocol
import signal
import sys
from ExTx.SuperAgent import SuperAgent, BeginningPrinter
from multiprocessing import Process
import os
import json


class BeginningPrinter(Protocol):
    def __init__(self, finished):
        self.finished = finished
        self.remaining = 1024 * 100
        self.body = ""

    def dataReceived(self, bytes):
        self.body += bytes

    def connectionLost(self, reason):
        print 'Finished receiving body:', reason.getErrorMessage()
        self.finished.callback(self.body)



class Store(object):
    def __init__(self, setting):
        self.setting = setting
        self.mongo_remote = None
        self.redis_remote = None
    
    @defer.inlineCallbacks
    def install(self):
        try:

            self.mongo = yield txmongo.MongoConnection(host=self.setting.MONGO_HOST, port=self.setting.MONGO_PORT)
            self.redis = yield txredisapi.Connection(host=self.setting.REDIS_HOST, port=self.setting.REDIS_PORT)
            yield self.mongo.admin.authenticate("root", "chenfuzhi")
            yield self.redis.auth("chenfuzhi")
            
            self.redis2 = yield txredisapi.Connection(host=self.setting.REDIS_HOST, port=self.setting.REDIS_PORT)
            yield self.redis2.auth("chenfuzhi")
            
            defer.returnValue(True)
        except Exception, e:
            log.err()
            defer.returnValue(False)


def wait(second):
    #log.msg("wait:%s secode......" % second)
    d = defer.Deferred()
    reactor.callLater(second, d.callback, None)
    return d
        
class BaseWorker(object):
    def __init__(self, store, task):
        self.store = store
        self.setting = store.setting
        self.task = task
        self.key_task = "%s:TASK:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.key_queue = "%s:QUEUE:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.key_success = "%s:SUCCESS:%s" % (self.setting.KEY_PRE, str(task["_id"]))
        self.srange = task["srange"]
        
        
@defer.inlineCallbacks
def test_proxy(ip_addr, port):
    agent = SuperAgent(proxy=[ip_addr, port])
    response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
    if response.code == 200:
        finished = defer.Deferred()
        response.deliverBody(BeginningPrinter(finished))
        response = yield finished
        if response.decode("gb2312").find(u"您的IP是：") > -1:
            defer.returnValue(response.decode("gb2312"))
    defer.returnValue(None)


def intip2str(intip):
    return socket.inet_ntoa(struct.pack('I',socket.htonl(int(intip))))
def strip2int(strip):
    return socket.ntohl(struct.unpack("I",socket.inet_aton(str(strip)))[0])


def por_start(*args):
    
    @defer.inlineCallbacks
    def start(*args):
        log.msg("begin install store")
        store = Store(setting)
        
        ret = yield store.install()
        if ret is False:
            log.err("store install error !!!")
            reactor.stop()
        else:
            log.msg("install successfully")
    
    
        por = yield args[0](store, *args[1:])
        por.start()

#        yield wait(86400*30)

#        self.seeders[task_id] = Process(target=seed.start) 
#        self.seeders[task_id].start()

    start(*args)
    reactor.run()

@defer.inlineCallbacks
def post_json(json_data):
    from twisted.web import client
    header = {
              "Content-Type":"application/json"
              }
    post_data = json.dumps(json_data)
    response = yield client.getPage("http://42.96.168.234:9090/input_ip/", 
                                    method="POST", 
                                    headers=header, 
                                    postdata=post_data)

    print response
    defer.returnValue(response) 

def save_status(data):

    import urllib2
    req = urllib2.Request('http://42.96.168.234:9090/save_status/')
    req.add_header("Content-Type", "application/json")
    r = urllib2.urlopen(req, data="{}")

    print r.read()
    

def save_ip(data):
    req = urllib2.Request('http://42.96.168.234:9090/input_ip/')
    req.add_header("Content-Type", "application/json")
    return urllib2.urlopen(req, data=json.dumps(data)).read()
                            
import urllib2
if __name__ == "__main__":
    data = {"uri":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"}
    print save_ip(data)
#    save_status({"data":"ddddddddddddddddddd"})
#    reactor.callWhenRunning(post_json, {"uri":"chenfuzhisssssssssssss"})
#    reactor.run()



