#coding: utf-8

import sys
import os


from twisted.python import log
from twisted.internet import reactor
from twisted.web.error import Error
from twisted.internet import defer
from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
from bson.objectid import ObjectId
from twisted.web.client import getPage

from lxml import etree
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Response, HtmlResponse
from scrapy.http.response.text import TextResponse

import urllib2, gzip, StringIO
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web.http_headers import Headers
import txmongo
import pymongo
import txredisapi
import time
from xml.etree import ElementTree
import urllib2
import re
import MySQLdb
import MySQLdb.cursors
import hashlib
import pymongo
import time
import urllib2
import socket
import struct
from twisted.web import client
import subprocess

g_int_ip = 0
g_ip_count = 0
key_ip_queue = "key_ip_queue"
run_mask_int = 0
run_ip_int = 0

@defer.inlineCallbacks
def input_ip_queue():
    global current_ip, g_int_ip, g_ip_count, run_mask_int, run_ip_int
    
    if conn_redis is None:
        reactor.callLater(3, input_ip_queue)
        defer.returnValue(None)
    
    ip_range_list = yield conn_redis.llen("ip_range_list")
    
    if ip_range_list == 0:

        import pymongo
        conn = pymongo.Connection("192.168.2.202", 27017)
        coll = conn["sys"]["china_ip"]
        rows = coll.find()
        ip_range = []
        for row in rows:
            ip_mask = "%s:%s" % (row["ip"], row["ip_mask"])
            yield conn_redis.lpush("ip_range_list", ip_mask)
#------------------------------------------------------------------------------ 

    run_mask_int = yield conn_redis.exists("run_mask_int")
    if run_mask_int == 0:
        ip_mask = yield conn_redis.lpop("ip_range_list")
        if ip_mask is None:
            print "finish input seed"
            defer.returnValue(None)
            
        ip, mask = ip_mask.split(":")
        int_ip = socket.ntohl(struct.unpack("I",socket.inet_aton(str(ip)))[0])
        int_mask = socket.ntohl(struct.unpack("I",socket.inet_aton(str(mask)))[0])
        yield conn_redis.set("run_ip_int", str(int_ip + 1))
        yield conn_redis.set("run_mask_int", str(int_mask))
        
        
    queue_size = yield conn_redis.llen(key_ip_queue)
    if queue_size < 100000:

        int_ip = yield conn_redis.get("run_ip_int")
        int_mask = yield conn_redis.get("run_mask_int")
        
        while (int(int_ip) + 1) < int(int_mask):
            yield conn_redis.incr("run_ip_int")
            ip = socket.inet_ntoa(struct.pack('I',socket.htonl(int(int_ip) + 1))) 
            yield conn_redis.lpush(key_ip_queue, ip)
            int_ip = yield conn_redis.get("run_ip_int")
            print "=====", ip
        else: 
            yield conn_redis.delete("run_ip_int")
            yield conn_redis.delete("run_mask_int")
            
        reactor.callLater(0, input_ip_queue)
    else:
        reactor.callLater(1, input_ip_queue)
        
    
conn_mongo = None
conn_redis = None

@defer.inlineCallbacks
def init_db():
    global conn_mongo, conn_redis
    conn_mongo = yield txmongo.MongoConnection(host="192.168.2.202", port=27017)
    conn_redis = yield txredisapi.Connection(host="192.168.2.202", port=6379)    


    
@defer.inlineCallbacks
def scan(scan_id):

    if conn_redis is None:
        reactor.callLater(3, scan, scan_id)
        defer.returnValue(None)
    host = yield conn_redis.lpop(key_ip_queue)

    if host is not None:
        try:
            endpoint = TCP4ClientEndpoint(reactor, host, 6666)
            agent = client.ProxyAgent(endpoint)
            response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
            if response.code == 200:
                finished = Deferred()
                response.deliverBody(BeginningPrinter(finished))
                response = yield finished
                if response.decode("gb2312").find(u"您的IP是：") > -1:
                    print response, "ok", 
                    yield conn_redis.lpush("successfully_proxy_ip", host)
                    print "successfull scan ip:%s" % host
        except Exception, e:
            print e
        
        yield conn_redis.incr("scan_count_num")
        reactor.callLater(0, scan, scan_id) 
    else:
        
        reactor.callLater(3, scan, scan_id) 



class ProxyClientFactory(client.HTTPClientFactory):
    def setURL(self, url):
        client.HTTPClientFactory.setURL(self, url)
        self.path = url

from multiprocessing import Process

def main():
    init_db()
    for i in range(1000):
        print i
        reactor.callLater(1, scan, "scan_%s" % i)
    reactor.run()
    
def seed():
    init_db()
    input_ip_queue()
    reactor.run()
#    url = "http://www.sf32.com/APISeara_act.php?username=chenfuzhi&pwd=chenfuzhi&num=1000&loc=%C8%AB%B9%FA&pro=%C8%AB%B2%BF&daili%5B%5D=%B3%AC%BC%B6%C4%E4%C3%FB&daili%5B%5D=%C6%D5%CD%A8%C4%E4%C3%FB&daili%5B%5D=%CD%B8%C3%F7%B4%FA%C0%ED&ports%5B%5D=8909&ports%5B%5D=6675&ports%5B%5D=6666&ports%5B%5D=8080&ports%5B%5D=808&ports%5B%5D=6668&commit=%CC%E1+%C8%A1&action=ok"
#
#    ips = urllib2.urlopen(url).read()
#    for l in ips.split("\n"):
#        redis.Redis("192.168.2.202", 6379).lpush(key_ip_queue, l.split(":")[0])
    
    
if __name__ == '__main__':
    
    p = Process(target=seed)
    p.start()
    
#    pros = []
#    for i in range(1):
#        p2 = Process(target=main)
#        p2.start()
#        pros.append(p2)
    
    
    
