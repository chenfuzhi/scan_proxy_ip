#coding: utf-8

import sys
import os

from twisted.python import log
from twisted.internet import reactor
from twisted.web.error import Error
from twisted.internet import defer
from twisted.internet.defer import Deferred, \
                                   DeferredList, \
                                   gatherResults, \
                                   returnValue, \
                                   inlineCallbacks
from bson.objectid import ObjectId
from twisted.web.client import getPage

from lxml import etree
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Response, HtmlResponse
from scrapy.http.response.text import TextResponse

import urllib2, gzip, StringIO
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.web.http_headers import Headers
import txmongo
import pymongo
import txredisapi
import time
from xml.etree import ElementTree
import urllib2
import re
import MySQLdb
import MySQLdb.cursors
import hashlib
import pymongo
import time
import urllib2
import socket
import struct
from twisted.web import client
import subprocess
from twisted.web.client import Agent, ProxyAgent
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.internet.protocol import Protocol
conn_mongo = None
conn_redis = None

class BeginningPrinter(Protocol):
    def __init__(self, finished):
        self.finished = finished
#        self.remaining = 1024 * 100
        self.body = ""

    def dataReceived(self, bytes):
        self.body += bytes

    def connectionLost(self, reason):
        print 'Finished receiving body:', reason.getErrorMessage()
        self.finished.callback(self.body)


@defer.inlineCallbacks
def test_scan():
    conn_redis = yield txredisapi.Connection(host="192.168.2.202", port=6379)   
    while 1:
#        host = yield conn_redis.rpoplpush("successfully_proxy_ip", "successfully_proxy_ip")
        host = yield conn_redis.rpop("successfully_proxy_ip")
        print "begin test url:%s" % host
        try:
            endpoint = TCP4ClientEndpoint(reactor, host, 6666)
            agent = client.ProxyAgent(endpoint)
            response = yield agent.request("GET", 'http://iframe.ip138.com/ic.asp')
            if response.code == 200:
                finished = Deferred()
                response.deliverBody(BeginningPrinter(finished))
                response = yield finished
                if response.decode("gb2312").find(u"您的IP是：") > -1:
                    print response.decode("gb2312"), "ok", 
                    print "successfull scan ip:%s" % host
                    yield conn_redis.lpush("successfully_proxy_ip", host)
        except Exception, e:
            print "err:%s" % e
        


class ProxyClientFactory(client.HTTPClientFactory):
    def setURL(self, url):
        client.HTTPClientFactory.setURL(self, url)
        self.path = url

    
if __name__ == '__main__':

    for i in range(100):
        reactor.callWhenRunning(test_scan)    
    reactor.run()
