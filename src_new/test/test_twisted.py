#coding:utf-8


from twisted.internet import reactor, defer
from twisted.web.client import getPage
from Queue import Queue

urls = Queue()


@defer.inlineCallbacks
def seed():
    
    while 1:
        if urls.qsize() < 1000:
            urls.put("http://search.51job.com/job/1,c.html")
        else:
            d = defer.Deferred()
            reactor.callLater(3, d.callback, None)
            yield d


@defer.inlineCallbacks
def main():
    
    while 1:
        url = urls.get()
        if url is not None:
            try:
                ret = yield getPage(url)
                print len(ret)
            except Exception, e:
                print e
        else:
            d = defer.Deferred()
            reactor.callLater(3, d.callback, None)
            yield d



@defer.inlineCallbacks
def init():
    
    reactor.callLater(0, seed)
    for i in range(10):
        reactor.callLater(0, main)
    
    defer.returnValue(None)
        

if __name__ == "__main__":
    
    
    reactor.callWhenRunning(init)
    reactor.run() 