#coding:utf-8

import pymongo
from bson.objectid import ObjectId

from flask import Flask

from flask.ext import admin, wtf

from flask.ext.admin.form import Select2Widget
from flask.ext.admin.contrib.pymongo import ModelView, filters
from flask.ext.admin.model import fields
import re
import json
from wtforms.fields import *
import os
import sys
import psutil
import subprocess
import setting
import Process


# Create application
app = Flask(__name__)

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'

# Create models
conn = pymongo.Connection()
conn.admin.authenticate("root", "chenfuzhi")
db = conn.tasks




# User admin
class FindForm(wtf.Form):
    type = SelectField(choices=[("mongo", "mongodb"), ("code", u"计算得到")])
    code = StringField()


class TaskForm(wtf.Form):
    name = wtf.TextField('name', description=u'输入任务名字...', default=u"XXXXXXXXXX")
    ports = fields.InlineFieldList(wtf.IntegerField(default=6666), description=u'端口',)
    
    queue = wtf.IntegerField("queue", description=u'队列长度', default=10000)
    limit = wtf.IntegerField("limit", description=u'查询大小', default=5000)
    prio = wtf.IntegerField("prio", description=u'优先级', default=0)
    weight = wtf.IntegerField("weight", description=u'权重', default=500)
    wait = wtf.IntegerField("wait", description=u'等待时间', default=300)
    runtimes = wtf.IntegerField("runtimes", description=u'运行次数', default=0)
    find = fields.InlineFormField(FindForm)
    
    aaaa = SelectField(choices=[("aaa", "aaa"), ("bbb", "bbb")])
    
    bbb = SelectMultipleField(choices=[("aaa", "aaa"), ("bbb", "bbb")])
    ccc = RadioField(choices=[("aaa", "aaa"), ("bbb", "bbb")])
    # Form list
    #form_list = fields.InlineFieldList(fields.InlineFormField(FindForm))

def redisplay(v, c, m, p):

    print c["row"]["_id"]
    return 1
    
def ch_find(v, c, m, p):
    print 1
    
    return c["row"]["find"]["type"]
    
    
class TaskView(ModelView):
    list_template = 'list.html'
    actions_template = 'actions.html'
    
    column_labels = {"console":u"控制台"}
    column_list = ('_id', 'name', 'ports', 'queue', "aaaa", "bbb", "find", "runtimes", "console", )             
    form = TaskForm
    column_formatters = dict(find=ch_find)
    
    
# Flask views
@app.route('/')
def index():
    return '<a href="/admin/">Click me to get to Admin!</a>'


from flask import request


# Flask views
@app.route('/task/')
def task_admin():
    ret = {"success":0}
    _id = request.args.get('tid', '')
    act = request.args.get('act', '')
    if _id == "" or act == "":
        return '{"success":0, "msg":"_id is none"}' 
        
    pid_path = "%s%s.pid" % (app.config["por_path"], _id)
    if act == "status":
        try:
            pid = open(pid_path, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        print "dddddd"
        try:
            args = ["/usr/local/bin/twistd",
                     "-y",
                     "/root/scan_proxy_ip/src/task_runner.py",
                     "--pidfile=%s" % pid_path,
                     "--logfile=/var/log/task_runner.log",
                     "--rundir=/root/scan_proxy_ip/src/",
                     "--chroot=/root/scan_proxy_ip/src/",
                     ]
            out = subprocess.call(args)
            print " ".join(args)
            print out, "oooooooooooooooo"
            if "Another twistd server is running" in out:
                return '{"success":0, "msg":"%s"}' % "Another twistd server is running"
            if out == "":
                return '{"success":1, "msg":"%s"}'
            
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "stop":
        try:
            pid = open(pid_path, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e


@app.route('/schdc/')
def schdc():
    act = request.args.get('act', '')
        
    if act == "status":
        return "ddddddddddddddd"
        try:
            pid = open(setting.SCHD_PID, 'r').read()
            p = psutil.Process(int(pid))
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "start":
        
        try:
            import schd
            from common import *
            
            p = Process(target=por_start, args=(schd.Task,))
            p.start()
            return '{"success":0, "msg":"%s"}' % e
        
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e
        
    elif act == "stop":
        try:
            pid = open(pid_path, 'r').read()
            p = psutil.Process(int(pid))
            p.terminate()
            return '{"success":1, "msg":"%s"}' % p.name
        except Exception, e:
            return '{"success":0, "msg":"%s"}' % e





admin = admin.Admin(app, u'扫描器后台')
# Add views
admin.add_view(TaskView(db.user, u'任务管理'))
app.config["por_path"] = "/tmp/pors/"
# Start app
app.debug = True


if __name__ == '__main__':
    # Create admin
    app.run('0.0.0.0', 8000)
    
if __name__=='__builtin__':
    pass

    
    