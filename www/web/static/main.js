$(function () {

Highcharts.setOptions({
    global:{
        useUTC:false
    }
    });

$.getJSON("/data/jobcatch/job",function(usdeur){
    var chart2 = new Highcharts.StockChart({
                chart: {
                    renderTo: 'container'
                },
                rangeSelector: {
                    selected: 1,
                    inputEnabled: false
                },
                
                navigator: {
                    height: 30
                },
                
                title: {
                    text: '职位-信息',
                    floating: true,
                    align: 'right',
                    x: -20,
                    top: 20
                },
                
                xAxis: {
                    maxZoom: 14 * 24 * 3600000 // fourteen days
                },
                
                // series: [{
                //     name: '职位_当天抓取数',
                //     data: usdeur,
                //     tooltip: {
                //         yDecimals: 2
                //     }
                // }]
                series:usdeur
            });
    });
$.getJSON("/data/jobcatch/company",function(usdeur){
    var chart2 = new Highcharts.StockChart({
                chart: {
                    renderTo: 'container2'
                },
                rangeSelector: {
                    selected: 1,
                    inputEnabled: false
                },
                
                navigator: {
                    height: 30
                },
                
                title: {
                    text: '公司-信息',
                    floating: true,
                    align: 'right',
                    x: -20,
                    top: 20
                },
                
                xAxis: {
                    maxZoom: 14 * 24 * 3600000 // fourteen days
                },
                
                series:usdeur
            });
    });
$('#querysum').click(function(){
    var startdate=$('#startdate').val()
       ,enddate=$('#enddate').val()
       ,type=$('#querytype').val()
       ,self=this;
    $.getJSON('/data/jobsum/'+type+'/'+startdate+'/'+enddate,function(data){
        console.log(data);
        var html=[];
        for(var key in data){
            html.push('<div class="result"><span class="left">');
            html.push(key);
            html.push(':</span><span class="right">');
            html.push(data[key]);
            html.push('</span></div>');
        }
        $('#sum').html(html.join(''))
    })
});
});
