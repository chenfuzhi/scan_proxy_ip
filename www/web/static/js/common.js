$(document).ready(function(){
	
	$('.btn-group[data-toggle="buttons-checkbox"] button').bind("click", function(){
		 var btn = $(this);
		 if (btn.hasClass("btn-link")){
			 btn.removeClass("btn-link");
		 }else{
			 btn.addClass("btn-link");
		 }
	});
	
	//console.log(btns.length);
});



function ajax_getip(){

	var ports = [];
	var provices = [];
	var types = [];
	var num = 0;
	
	$('#ctrls_port input[type="checkbox"]').each(function(){
		if(this.checked == true)
		   ports.push($(this).next().attr("title"));
	});
	
	$('#ctrls_area input[type="checkbox"]').each(function(){
		if(this.checked == true)
			provices.push($(this).next().attr("title"));
	});
	
	$('#ctrls_type input[type="checkbox"]').each(function(){
		if(this.checked == true)
			types.push($(this).next().attr("title"));
	});
	
	console.log(ports);
	console.log(provices);
	console.log(types);
	
	var num = $("#get_num").val();
	var delay = $("#get_delay").val();
	var tid = $("#get_tid").val();
	
	var is_ok = true;
	$("#alert-div").hide();
	
	if(ports.length == 0){
		$("#alert-div").html("没有选择端口!!");
		$("#alert-div").show();
		is_ok = false;
	}
	if(provices.length == 0){
		$("#alert-div").html("没有选择省份!!");
		$("#alert-div").show();
		is_ok = false;
	}
	if(types.length == 0){
		$("#alert-div").html("没有选择运营商!!");
		$("#alert-div").show();
		is_ok = false;
	}
	console.log(num);
	if(isNaN(parseInt(num))){
		$("#alert-div").html("提取数量填写不正确!!");
		$("#alert-div").show();
		is_ok = false;
	}

	if(isNaN(parseInt(delay))){
		$("#alert-div").html("等待时间填写不正确!!");
		$("#alert-div").show();
		is_ok = false;
	}
	
	if(isNaN(parseInt(tid))){
		$("#alert-div").html("请填写正确的订单号!!");
		$("#alert-div").show();
		is_ok = false;
	}
	
	console.log(is_ok);
	if(is_ok == true){
		alert("验证成功");
		console.log(ports);
		console.log(provices);
		console.log(types);
		console.log(num);
		console.log(delay);
		
		var args = { ports: ports, 
					 areas: provices, 
					 types: types,
					 num: num,
					 delay: delay,
					 tid:tid
				   };
		
		if($("#ctrls_port input[type='checkbox']").length == ports.length){
			args.ports = [];
		}

		if($('#ctrls_area input[type="checkbox"]').length == provices.length){
			args.areas = [];
		}
		
		if($('#ctrls_type input[type="checkbox"]').length == types.length){
			args.types = [];
		}
		
		$("#get_info").html("");
		$.get("/api/extract/", args,
		  function(data){
		  	$("#get_info").html(data.replace(/\n/g, "<br/>"));
		  	$("#api_url").text("http://42.96.168.234/api/extract/?" + $.param(args));
		});
	}

	
}