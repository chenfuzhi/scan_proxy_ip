#coding:utf-8
import pymongo
from bson.objectid import ObjectId

from flask import Flask
from flask import request
from flask.ext import admin, wtf
from web import app
from flask.ext.admin.form import Select2Widget
from flask.ext.admin.contrib.pymongo import ModelView, filters
from flask.ext.admin.model import fields
from twisted.internet import *

from flask import render_template
import pymongo
import txmongo
from bson.json_util import default
import json
import time

# Flask views

#@app.route('/getip/')
#def getip():
#    return render_template('getip.htm', name="ddddddd")


@app.route('/api/tongji/')
def tongji():
    tongji = app.conn["data"]["tongji"]
    rows = tongji.find(sort=[("ts", 1)])
    
    rets = []
    ip_all = {
              "name": u"代理IP总数",
              "data":[]
              }
    
    ports = {}
    for row in rows:
        for k, v in row["ports"].iteritems():
            if ports.has_key(k):
                ports[k]["data"].append([row["ts"], v])
            else:
                ports[k] = {
                             "name": u"端口%s" % k,
                             "data":[]
                            }
            
        ip_all["data"].append([row["ts"], row["sum"]])
    
    
    rets.append(ip_all)
    for k, v in ports.iteritems():
        rets.append(v)
        
    print rets
    
    return json.dumps(rets, default=default)
    

@app.route('/api/extract/')
def extract_proxy_ip():
    areas = request.args.getlist("areas[]")
    types = request.args.getlist("types[]")
    ports = request.args.getlist("ports[]")
    num = request.args.get("num")
    delay = request.args.get("delay")
    tid = request.args.get("tid")
    
    tid, delay, num = map(int, [tid, delay, num])
    print areas
    print types
    print ports
    print num
    print delay
    print tid
    
    row = app.conn.data.order.find_one({"tid":tid}, limit=1)
    if row is None:
        return "订单号不存在"
    row = dict(row)
    
    

    if row["type"] == "count":
        max = row["num"] * row["ips"]
        
        if row.has_key("used"):
            if row["used"] > max:
                return "你的数量已经用完"
            elif row["used"] >= max - num:
                return "你已经没那么多ip提取了, 只有:%s个了" % (max - row["used"] - 1)

#                 return "你可以提取IP了"
    #===================================================================
    # 
    #===================================================================
        query = {
                 "status":"o",
                 }
        
        if len(areas) > 0:
            query["province"] = {"$in":areas}
        if len(ports) > 0:
            query["port"] = {"$in":map(int, ports)}
        if len(types) > 0:
            query["type"] = {"$in":types}
        
        rows = app.conn37017.data.ip.find(query)
        rows = list(rows)
        
    #------------------------------------------------------------------------------ 判断重复，和使用时间
        rets = []
        
        if delay > 0:
            
            query = {"tid":tid, 
                     }
            use_logs = app.conn["data"]["order"].find_one(query, limit=1)         
            use_logs = dict(use_logs)
            
            if use_logs.has_key("logs"):
                ips = [l["ip"] for l in use_logs["logs"] if l["ts"] > (int(time.time()) - delay*60)]
                for r in rows:
                    if r["ip"] not in ips:
                        rets.append(r)
            else:
                rets = rows
        else:
            pass
        
        if len(rets) == 0:
            return "没有更多符合条件的IP了"
        
        if len(rets) > num:
            rets = rets[0:num]
            
    #------------------------------------------------------------------------------ 改变订单账户信息
        vals = []
        for r in rets:
            vals.append({"ip":r["ip"], "ts":int(time.time())})
        app.conn["data"]["order"].update({"tid":tid}, 
                                      {
                                       "$inc":{"used":len(vals)}, 
                                       "$pushAll":{"logs":vals}
                                       }
                                      )
    
        ret_str = []
        for r in rets:
            if not r.has_key("province"):
                r["province"] = u"未知"
            if not r.has_key("type"):
                r["type"] = u"未知"
            s = "%s:%s|%s|%s" % (r["ip"], r["port"], r["type"], r["province"])
            ret_str.append(s)
        return "\n".join(ret_str)
    
    
    return "错误。。。。。"

