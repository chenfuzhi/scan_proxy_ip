import pymongo
from bson.objectid import ObjectId

from flask import Flask

from flask.ext import admin, wtf
from web import app
from flask.ext.admin.form import Select2Widget
from flask.ext.admin.contrib.pymongo import ModelView, filters
from flask.ext.admin.model import fields
from twisted.internet import *

from flask import render_template
from jinja2 import Template
import pymongo
import txmongo
from twisted.python import log

#Flask views

#@app.route('/getip/')
#def getip():
#return render_template('getip.htm', name="ddddddd")


@app.route('/')
def index():    
    return render_template('index.html')
    
@app.route('/help/')
def help():    
    return render_template('help.html')
    

@app.route('/getip/')
def getip():
    db_ip = app.conn37017["data"]["ip"]
    ips = db_ip.find({"status":"o"})
    ports = ips.distinct("port")
    types = ips.distinct("type")
    provinces = ips.distinct("province")
    print ports
    print types
    print provinces
    
    data = {}
    data["sum"] = db_ip.count()
    data["ports"] = {}
    data["types"] = {}
    data["provinces"] = {}
    
    for p in ports:
        data["ports"][str(p)] = db_ip.find({"status":"o", "port":p}).count()
        
    for t in types:
        data["types"][unicode(t)] = db_ip.find({"status":"o", "type":t}).count()
    
    for p in provinces:
        data["provinces"][unicode(p)] = db_ip.find({"status":"o", "province":p}).count()
        
    return render_template('getip.html', **data)


if __name__ == '__main__':
    # Create admin
    admin = admin.Admin(app, 'Simple Models')

    # Add views
    admin.add_view(UserView(db.user, 'User'))
    admin.add_view(TweetView(db.tweet, 'Tweets'))

    # Start app
    app.debug = True
    app.run('0.0.0.0', 8000)
    