import pymongo
from bson.objectid import ObjectId

from flask import Flask

from flask.ext import admin, wtf
from web import app
from flask.ext.admin.form import Select2Widget
from flask.ext.admin.contrib.pymongo import ModelView, filters
from flask.ext.admin.model import fields

# Create application
#app = Flask(__name__)

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'


# User admin
class RangeForm(wtf.Form):
    begin = wtf.TextField('begin')
    end = wtf.TextField('end')


class TaskForm(wtf.Form):
    name = wtf.TextField('name')
    port = wtf.IntegerField('port')
    status = wtf.TextField('status', default='ready')
    run_times = wtf.IntegerField('run_times', default=0)
    # Form list
    srange = fields.InlineFieldList(fields.InlineFormField(RangeForm))


class TaskView(ModelView):
    column_list = ('name', 'port', 'status', 'run_times')
    column_sortable_list = ('name')
    form = TaskForm


if __name__ == '__main__':
    # Create admin
    admin = admin.Admin(app, 'Simple Models')

    # Add views
    admin.add_view(UserView(db.user, 'User'))
    admin.add_view(TweetView(db.tweet, 'Tweets'))

    # Start app
    app.debug = True
    app.run('0.0.0.0', 8000)
    