#coding:utf-8

import pymongo
import time
from bson.objectid import ObjectId
from flask.ext.script import Manager
import json
from web import app
from bson.json_util import *
manager = Manager(app)

@manager.command
def hello():
    print "hello"

@manager.command
def tongji():
    db_ip = app.conn37017["data"]["ip"]
    ips = db_ip.find({"status":"o"})
    ports = ips.distinct("port")
    types = ips.distinct("type")
    provinces = ips.distinct("province")
    data = {}
    data["ts"] = int(time.time())
    data["sum"] = db_ip.count()
    data["ports"] = {}
    data["types"] = {}
    data["provinces"] = {}
    
    for p in ports:
        data["ports"][str(p)] = db_ip.find({"status":"o", "port":p}).count()
        
    for t in types:
        data["types"][unicode(t)] = db_ip.find({"status":"o", "type":t}).count()
    
    for p in provinces:
        data["provinces"][unicode(p)] = db_ip.find({"status":"o", "province":p}).count()
        
    app.conn.data.tongji.save(data)
    
    return json.dumps(data, indent=4, default=default)

    
    

if __name__ == "__main__":
    manager.run()